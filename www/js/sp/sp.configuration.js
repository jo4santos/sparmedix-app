sp.debug.register("start");
sp.configuration = {};
sp.configuration.modal = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};

	$(".sp-configuration-modal").remove();
	sp.loading.show({text:"A obter configurações"});
	var modal_id = "sp-configuration-modal-"+sp.users.last_id++;
	var modal = $('\
	<div class="modal sp-configuration-modal" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header" style="border-bottom:0;">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Configuração</h4>\
				</div>\
				<div class="modal-body" style="padding:0;"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer" style="border-top:0;">\
					<button type="button" class="btn btn-primary sp-configuration-form-confirm"><i class="fa fa-check"></i> Guardar</button>\
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var str = '';
	str += '<table class="table table-striped table-bordered" style="margin:0;">';

	str += '<tr>';
	str += '	<td class="sp-checkbox">';
	str += '		<input class="sp-configuration-email_notifications" type="checkbox" '+(sp.user.configs.email_notification.value=="true"?"checked":"")+' /> Receber notificações de pagamentos por e-mail';
	str += '	</td>';
	str += '</tr>';

	str += '<tr>';
	str += '	<td class="sp-checkbox">';
	str += '		<input class="sp-configuration-automatic_sync" type="checkbox" '+(sp.user.configs.automatic_sync.value=="true"?"checked":"")+' /> Sincronizar automaticamente encomendas pendentes ao obter ligação';
	str += '	</td>';
	str += '</tr>';

	str += '</table>';
	modal.find(".modal-body").html(str);
	
	modal.find(".sp-configuration-form-confirm").on("click",function() {
		var configs = new Array();
		configs.push({"name":"email_notification","value":(modal.find(".modal-body .sp-configuration-email_notifications").is(":checked")?"true":"false")});
		configs.push({"name":"automatic_sync","value":(modal.find(".modal-body .sp-configuration-automatic_sync").is(":checked")?"true":"false")});
		sp.db.user.update_configs(configs);
		modal.modal("hide");
	});

	$(".modal:visible").modal("hide");
	
	modal.modal("show");
	sp.loading.hide();
};
sp.debug.register("end");