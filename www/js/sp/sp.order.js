sp.debug.register("start");

sp.order = {
	client: null,
	bill_client: null,
	list: new Array(),
	entidade: 11202,
	subentidade: 296,
    activeSelect: "" // final,bill
};
sp.order.add = function (id,quantity,bonus,discount) {
	var found = false;
	var product = sp.db.products.get_details(id);
	for(var i in sp.order.list) {
		if(sp.order.list[i].id == id) {
			sp.order.list[i].quantity=parseInt(quantity);
			sp.order.list[i].bonus=parseInt(bonus);
            sp.order.list[i].discount=parseInt(discount);
			found = true;
		}
	}
	if(!found) {
		sp.order.list.push({id:id,quantity:parseInt(quantity),bonus:parseInt(bonus),discount:parseInt(discount)});
	}
	toastr.success("<span class='toastr-label'>Produto:</span> "+product.nome+"<br><span class='toastr-label'>Quantidade:</span> "+quantity+" + "+bonus+" bonus"+"<br><span class='toastr-label'>Desconto:</span> "+discount+"%<br><span class='toastr-label'>Preço:</span> "+((1-parseInt(discount)/100)*parseInt(quantity)*sp.db.products.get_price(product.id))+"&euro;","Adicionado à encomenda");
	sp.order.update();
}
sp.order.add.note = function (id) {
	var details = sp.db.notes.get_details(id);
	sp.order.client = details.client_id;
    sp.order.bill_client = details.payee_id;
	var products = sp.db.notes.get_products(id);
	var counter = 0;
	for(var i in products) {
		if(sp.db.products.get_details(products[i].product_id)) {
			sp.order.add(products[i].product_id,products[i].quantidade,products[i].bonus,products[i].discount);
			counter++;
		}
	}
	if(counter==0) {
		toastr.info("Os produtos da nota selecionada não se encontram disponíveis");
	}
}
sp.order.remove = function (id) {
	var product = sp.db.products.get_details(id);
	for(var i in sp.order.list) {
		if(sp.order.list[i].id == id) {
			sp.order.list.splice(i, 1);
			break;
		}
	}
	toastr.success("<span class='toastr-label'>Produto:</span> "+product.nome,"Removido da encomenda");
	sp.order.update();
}
sp.order.update = function () {
	$("#shopping-cart-modal-products,#shopping-cart-products span").empty();
	if(sp.user.tipo == 2) sp.order.client = sp.user.client_id;
	var preco_total = 0;
	var quantidade_total = 0;
	for(var i in sp.order.list) {
		var order = sp.order.list[i];
		var product = sp.db.products.get_details(order.id);
		
		var preco = ((1-parseInt(order.discount)/100)*parseInt(order.quantity)*sp.db.products.get_price(product.id));
		
		var text = '';

		text += '<tr>';
		text += '<td>';
		text += '<img class="sp-product-image-popup" src="'+sp.db.images.get(product.codigo+".jpg")+'" data-image="'+product.codigo+".jpg"+'" style="cursor:pointer;" />';
		text += '</td>';
		text += '<td>';
		text += ''+product.nome+'<br>';
		text += '<span class="label label-default">'+product.tipo_codigo+":"+product.codigo+'</span>';
        text += '<span class="label label-primary">'+order.bonus+' bonus</span>';
        text += '<span class="label label-success">-'+order.discount+'%</span>';
		text += '</td>';
		text += '<td>';
		text += ''+sp.db.products.get_price(product.id,true)+'&euro;';
		text += '</td>';
		text += '<td>';
		text += '<button onclick="location.href=\'##modal#product#'+product.id+'#open\';" class="btn btn-default sp-product-edit btn-sm">'+order.quantity+' <i class="fa fa-pencil"></i></button>';
		//text += '<a href="#" class="sp-product-remove" data-id="'+product.id+'"><small>Remover</small></a>';
		text += '</td>';
		text += '<td>';
		text += '<span class="label label-success" style="display:block;font-size:1em;padding:.6em .6em .5em;">'+sp.utils.format_price(preco)+' &euro;</span>';
		text += '</td>';
		text += '</tr>';
		
		$("#shopping-cart-modal-products").append(text);
		if($("#shopping-cart-products span").html()!="")$("#shopping-cart-products span").append(", ");
		$("#shopping-cart-products span").append(order.quantity+"x"+product.nome);
		
		preco_total += parseFloat(preco);
		quantidade_total += parseInt(order.quantity);
	}
	$(".sp-shopping-cart-badge-quantity").html(quantidade_total);
	$(".sp-shopping-cart-badge-price").html(sp.utils.format_price(preco_total)+"&euro;");
	if(sp.order.list.length==0) $("#shopping-cart-products span").html("Nenhum produto");
	$("#shopping-cart-modal-total-price").html(sp.utils.format_price(preco_total)+"&euro;");
	$("#shopping-cart-price span").html(sp.utils.format_price(preco_total)+"&euro;");

    $("#shopping-cart-modal-client").empty();
    if(sp.order.client!=null)
    {
        var client = sp.db.client.get_details(sp.order.client);

        $("#shopping-cart-modal-client").append('<tr><th><b>Nome</b></th><td class="sp-nome"><a href="##modal#client#'+client.id+'#open">'+client.nome+'</a></td></tr>');
        $("#shopping-cart-modal-client").append('<tr><th><b>NIF</b></th><td class="sp-nif">'+client.nif+'</td></tr>');
        $("#shopping-cart-client span").html(client.nome);
    }
    else
    {
        $("#shopping-cart-client span").html("Nenhum cliente seleccionado");
    }

    $("#shopping-cart-modal-bill_client").empty();
    if(sp.order.bill_client!=null)
    {
        var client = sp.db.client.get_details(sp.order.bill_client);

        $("#shopping-cart-modal-bill_client").append('<tr><th><b>Nome</b></th><td class="sp-nome"><a href="##modal#client#'+client.id+'#open">'+client.nome+'</a></td></tr>');
        $("#shopping-cart-modal-bill_client").append('<tr><th><b>NIF</b></th><td class="sp-nif">'+client.nif+'</td></tr>');
        $("#shopping-cart-client span").html(client.nome);

        $(".shopping-cart-modal-remove-bill_client").show();
    }
    else
    {
        $("#shopping-cart-client span").html("Nenhum cliente seleccionado");
        $(".shopping-cart-modal-remove-bill_client").hide();
    }
	if((sp.order.bill_client==null && sp.order.client==null && sp.order.list.length==0) || window.location.hash == "#order")
	{
		$("#shopping-cart").hide();
		$("html").removeClass("cart-active");
	}
	else
	{
		if(!(sp.user.tipo == "2" && sp.order.list.length==0)) {
			$("#shopping-cart").show();
			$("html").addClass("cart-active");
		}
		else {
			$("#shopping-cart").hide();
			$("html").removeClass("cart-active");
		}
	}
	
	$(".sp-product-remove").on("click",function(e){
		e.preventDefault();
		var product_id = $(this).attr("data-id");
			
		bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Pretende remover este produto da encomenda?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						sp.order.remove(product_id);
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});
};
sp.order.create = function() {
	if(sp.order.client==null && sp.order.list.length==0)
	{
		window.location="##modal#order#open";
	}
	else
	{
		if(sp.user.tipo == "2" && sp.order.list.length==0)
			window.location="##modal#order#open";
		else
			bootbox.dialog({
				message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Já existe uma encomenda em curso, por favor confirme que pretende removê-la para criar uma nova.</div>",
				title: "Confirmar",
				buttons: {
					success: {
						label: "<i class='fa fa-check'></i> Confirmar",
						className: "btn-primary",
						callback: function() {
							sp.order.client = null;
							sp.order.list = new Array();
							sp.order.update();
							window.location="##modal#order#open";
						}
					},
					danger: {
						label: "<i class='fa fa-times'></i> Cancelar",
						className: "btn-default"
					}
				}
			});
	}
};
sp.order.confirm = function() {
	if(!sp.order.client) {
		toastr.warning("Seleccione o cliente final","",{
            onclick:function(){
                sp.order.activeSelect = "final";
                window.location="#clients";
            }
        });
		return;	
	}
	if(sp.order.list.length==0) {
		toastr.warning("Adicione pelo menos produto à encomenda","",{onclick:function(){window.location="#products";}});
		return;
	}
	window.location="#order";
};
sp.order.cancel = function() {
	if(sp.order.client==null && sp.order.bill_client==null && sp.order.list.length==0)
	{
		window.location="##";
	}
	else
	{
		bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Por favor confirme que pretende cancelar a nova encomenda.<br><br>Todas as informações relativas à encomenda serão removidas!</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						sp.order.client = null;
                        sp.order.bill_client = null;
						sp.order.list = new Array();
						toastr.success("Encomenda cancelada");
						sp.order.update();
						window.location="##";
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	}
};
sp.order.generate_mb = function() {
	if(sp.order.bill_client!=null && sp.order.client != sp.order.bill_client) {
		sp.order.referencia = "";
		return true;
	}
	var valor = StringFormatVerify(sp.order.valor);
	if ((isNaN(sp.order.entidade)) || (String(sp.order.entidade).length != 5))
	{
		alert('Entidade inválida!');
		return false;
	}
	if ((isNaN(sp.order.subentidade)) || (String(sp.order.subentidade).length > 3) || (String(sp.order.subentidade) == "")) {
		alert('SubEntidade inválida!');
		return false;
	}
	if ((isNaN(sp.order.client)) || (String(sp.order.client).length > 4) || (String(sp.order.client) == "")) {
		alert('ID inválido!');
		return false;
	}
	if (isNaN(valor)) {
		alert('Valor inválido!');
		return false;
	}
	else if (valor <= 0) {
		alert('Valor inválido!');
		return false;
	}
	valor = formatAsMoney(valor);
	sp.order.referencia = GetPaymentRef(sp.order.entidade, sp.order.subentidade, sp.order.client, sp.order.valor);
	return true;
};

sp.debug.register("end");