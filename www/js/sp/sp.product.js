sp.debug.register("start");

sp.product = {last_id:0};
sp.product.add = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	if(options.id==-1)
	{
		console.error("[sp][sp.product.add()] invalid client id");
		return false;
	}
	$(".sp-product-add").remove();
	sp.loading.show({text:"A obter informações do produto"});
	var modal_id = "sp-product-add-"+sp.product.last_id++;
	var modal = $('\
	<div class="modal sp-product-add" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label"> Adicionar ao carrinho</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer">\
					<button type="button" class="btn btn-primary sp-product-add-confirm"><i class="fa fa-shopping-cart"></i> Confirmar</button>\
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);

	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});

	var product = sp.db.products.get_details(options.id);
	var quantity = 0;
	var bonus = 0;
    var discount = 0;
	for(var i in sp.order.list) {
		if(product.id==sp.order.list[i].id) {
			quantity=sp.order.list[i].quantity;
            bonus=sp.order.list[i].bonus;
            discount=sp.order.list[i].discount;
		}
	}

	modal.find(".modal-title").html('<img class="sp-product-image-popup" src="'+sp.db.images.get(product.codigo+".jpg")+'" height="49px" data-image="'+product.codigo+".jpg"+'" style="margin:-18px 5px -14px -14px;cursor:pointer;" /> Adicionar ao carrinho');

	var str = '';
	str += '	<table class="table table-striped table-bordered">';
	str += '		<tr><th><b>Código</b></th><td class="sp-codigo">'+product.tipo_codigo+" "+product.codigo+'</td></tr>';
	str += '		<tr><th><b>Descrição</b></th><td class="sp-nome">'+product.nome+'</td></tr>';
	str += '		<tr><th><b>Família</b></th><td class="sp-familia">'+(product.familia==""?"n/a":product.familia)+'</td></tr>';
	str += '		<tr><th><b>Preço unitário</b> <small>(s/IVA)</small></th><td class="sp-preco">'+sp.db.products.get_price(product.id,true)+' &euro;<input type="hidden" value="'+sp.db.products.get_price(product.id)+'"></td></tr>';
	str += '		<tr><th><b>IVA</b></th><td>'+product.iva+'%<input type="hidden" value="'+product.iva+'"></td></tr>';
	str += '	</table>';
	str += '	<h5 class="sp-header"><i class="fa fa-list-alt"></i> Encomenda</h5>';
	str += '	<table class="table table-striped table-bordered">';
	str += '		<tr>';
	str += '			<th style="width: 30%;vertical-align:middle;"><label for="sp-product-add-quantity'+modal_id+'"><b>Quantidade</b></label></th>';
	str += '			<td>';
	str += '				<div class="input-group">';
	str += '					<span class="input-group-addon btn btn-default sp-product-add-quantity-minus"><i class="fa fa-minus"></i></span>';
	str += '					<input type="text" class="form-control text-center sp-product-add-quantity-input" value="'+quantity+'" id="sp-product-add-quantity'+modal_id+'" />';
	str += '					<span class="input-group-addon btn btn-default sp-product-add-quantity-plus"><i class="fa fa-plus"></i></span>';
	str += '				</div>';
	str += '				<div class="btn-group btn-group-justified sp-product-add-quantity-presets" style="margin-top:5px;">';
	str += '					<a class="btn btn-default">2</a>';
	str += '					<a class="btn btn-default">5</a>';
	str += '					<a class="btn btn-default">10</a>';
	str += '					<a class="btn btn-default">20</a>';
	str += '				</div>';
	str += '			</td>';
	str += '		</tr>';
	str += '		<tr>';
	str += '			<th style="width: 30%;vertical-align:middle;"><label for="sp-product-add-bonus'+modal_id+'"><b>Bonus</b></label></th>';
	str += '			<td>';
	if(sp.user.tipo==2) {
		str += '					<input type="text" disabled="disabled" class="form-control text-center sp-product-add-bonus-input '+(bonus!=0?"first":"")+'" value="'+bonus+'" id="sp-product-add-bonus'+modal_id+'" />';
	} else {
        str += '				<div class="input-group">';
        str += '					<span class="input-group-addon btn btn-default sp-product-add-bonus-minus"><i class="fa fa-minus"></i></span>';
        str += '					<input type="text" class="form-control text-center sp-product-add-bonus-input '+(bonus!=0?"first":"")+'" value="'+bonus+'" id="sp-product-add-bonus'+modal_id+'" />';
        str += '					<span class="input-group-addon btn btn-default sp-product-add-bonus-plus"><i class="fa fa-plus"></i></span>';
        str += '				</div>';
	}
	str += '			</td>';
	str += '		</tr>';
    if(sp.user.tipo!=2) {
        str += '		<tr>';
        str += '			<th style="width: 30%;vertical-align:middle;"><label for="sp-product-add-discount' + modal_id + '"><b>Desconto</b></label></th>';
        str += '			<td>';
        str += '				<div class="input-group">';
        str += '					<span class="input-group-addon btn btn-default sp-product-add-discount-minus"><i class="fa fa-minus"></i></span>';
        str += '					<input type="text" class="form-control text-center sp-product-add-discount-input" value="' + discount + '" id="sp-product-add-discount' + modal_id + '" />';
        str += '					<span class="input-group-addon btn btn-default sp-product-add-discount-plus"><i class="fa fa-plus"></i></span>';
        str += '				</div>';
        str += '			</td>';
        str += '		</tr>';
        str += '		<tr>';
    }
	str += '		<tr><td colspan="2" style="height:3px;padding:0px;background:#dddddd"></td></tr>';
	str += '		<tr><th><b>Total</b> <small>(s/IVA)</small></th><td><span class="label label-success sp-product-add-quantity-total" style="font-size: 1.2em;display:block;"></span></td></tr>';
	str += '</table>';
	modal.find(".modal-body").html(str);

	modal.find(".modal-body .sp-product-add-quantity-input,.modal-body .sp-product-add-bonus-input,.modal-body .sp-product-add-discount-input").on("keypress",function(evt){
		var theEvent = evt || window.event;
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode( key );
		var regex = /[0-9]/;
		if( !regex.test(key) ) {
			theEvent.returnValue = false;
			if(theEvent.preventDefault) theEvent.preventDefault();
		}
	});

	modal.find(".modal-body .sp-product-add-quantity-minus").on("click",function(){
		var input = modal.find(".modal-body .sp-product-add-quantity-input");
		if(input.val()>0)input.val(parseInt(input.val())-1).trigger("change");
	});
	modal.find(".modal-body .sp-product-add-quantity-plus").on("click",function(){
		var input = modal.find(".modal-body .sp-product-add-quantity-input");
		input.val(parseInt(input.val())+1).trigger("change");
	});
	modal.find(".modal-body .sp-product-add-quantity-presets a.btn").on("click",function(){
		var value = parseInt($(this).html());
		var input = modal.find(".modal-body .sp-product-add-quantity-input");
		input.val(parseInt(input.val())+value).trigger("change");
	});
	modal.find(".modal-body .sp-product-add-quantity-total").on("update-value",function(){
		var unit_price = modal.find(".modal-body .sp-preco input").val()*100;
		var quantity = modal.find(".modal-body .sp-product-add-quantity-input").val();
        var discount = modal.find(".modal-body .sp-product-add-discount-input").val();
		if(!modal.find(".modal-body .sp-product-add-bonus-input").hasClass("first"))
		{
			if(product.bonus!=0)
				modal.find(".modal-body .sp-product-add-bonus-input").val(Math.floor(quantity/product.bonus));
			else
				modal.find(".modal-body .sp-product-add-bonus-input").val(0);
		}
		modal.find(".modal-body .sp-product-add-bonus-input").removeClass("first");
		var final_price = unit_price*parseInt(quantity)*(1-parseInt(discount)/100);
		final_price = final_price/100;
		$(this).html(sp.utils.format_price(final_price)+" &euro;");
	}).trigger("update-value");

    modal.find(".modal-body .sp-product-add-bonus-minus").on("click",function(){
        var input = modal.find(".modal-body .sp-product-add-bonus-input");
        if(input.val()>1)input.val(parseInt(input.val())-1).trigger("change");
    });
    modal.find(".modal-body .sp-product-add-bonus-plus").on("click",function(){
        var input = modal.find(".modal-body .sp-product-add-bonus-input");
        input.val(parseInt(input.val())+1).trigger("change");
    });

    modal.find(".modal-body .sp-product-add-discount-minus").on("click",function(){
        var input = modal.find(".modal-body .sp-product-add-discount-input");
        if(input.val()>4)input.val(parseInt(input.val())-5).trigger("change");
    });
    modal.find(".modal-body .sp-product-add-discount-plus").on("click",function(){
        var input = modal.find(".modal-body .sp-product-add-discount-input");
        if(input.val()<96)input.val(parseInt(input.val())+5).trigger("change");
    });
    modal.find(".modal-body .sp-product-add-discount-input").change(function(){
        var input = modal.find(".modal-body .sp-product-add-discount-input");
        if(input.val()<0)input.val(0);
        if(input.val()>100)input.val(100);
        modal.find(".modal-body .sp-product-add-quantity-total").trigger("update-value");
    });

	modal.find(".modal-body .sp-product-add-quantity-input").change(function(){
		modal.find(".modal-body .sp-product-add-quantity-total").trigger("update-value");
	});
	modal.find(".modal-footer .sp-product-add-confirm").on("click",function(){
		var quantity = modal.find(".modal-body .sp-product-add-quantity-input").val();
        var bonus = modal.find(".modal-body .sp-product-add-bonus-input").val();
        var discount = modal.find(".modal-body .sp-product-add-discount-input").val();

		var regex = /[0-9]/;
		if( !sp.utils.validate.number(quantity) || parseInt(quantity)<0 ) {
			toastr.error("Quantidade inválida. Apenas números superiores ou iguais a 0 são permitidos.");
			return false;
		}
        if( !sp.utils.validate.number(bonus) || parseInt(bonus)<0 ) {
            toastr.error("Quantidade de bonus inválida. Apenas números superiores ou iguais a 0 são permitidos.");
            return false;
        }
        if( !sp.utils.validate.number(discount) || parseInt(discount)<0 ) {
            toastr.error("Valor do desconto inválido. Apenas números compreendidos entre 0 e 100 são permitidos.");
            return false;
        }

		if(quantity==0)
		{
			var found = false;
			for(var i in sp.order.list) {
				if(sp.order.list[i].id == product.id) {
					found = true;
				}
			}
			if(found===true) {
				bootbox.dialog({
					message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Pretende remover este produto da encomenada?</div>",
					title: "Confirmar",
					buttons: {
						success: {
							label: "<i class='fa fa-check'></i> Confirmar",
							className: "btn-primary",
							callback: function() {
								sp.order.remove(product.id);
								window.history.back();
							}
						},
						danger: {
							label: "<i class='fa fa-times'></i> Cancelar",
							className: "btn-default"
						}
					}
				});
			}
			else {
				window.location = "##";
			}
		}
		else
		{
			sp.order.add(product.id,quantity,bonus,discount);
			window.location = "##";
			$("#sp-product-list-load-page").trigger("change");
		}
	});

	$(".modal:visible").modal("hide");

	modal.modal("show");
	sp.loading.hide();
};

sp.product.list = function(products) {
	options = {};
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	sp.loading.show({text:"A obter informações do utilizador"});
	$(".sp.product.list").remove();
	var modal_id = "sp.product.list-"+sp.product.last_id++;
	var modal = $('\
	<div class="modal sp.product.list" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Produtos</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);

	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});

	var str = '';
	str += '<table class="table table-striped table-bordered table-condensed" style="margin:0">';
	str += '<thead>';
	str += '<tr><th colspan="2"><small><b>Nome</b></small></th><th><small><b>Código</b></small></th></tr>';
	str += '</thead>';
	str += '<tbody>';
	for ( var i in products ) {
		var product = sp.db.products.get_details(products[i]);
		if(!product) {
			str += "<tr><td colspan='3'><small style='display:block;margin:0;padding:.4em 1em;' class='alert alert-warning'><i style='font-size:14px;margin-right:1em;' class='fa fa-warning'></i> Produto não encontrado</small></td></tr>";
			continue;
		}
		str += "<tr>";
		str += '<td style="width:51px;vertical-align:middle;" class="text-center"><img src="'+sp.db.images.get(product.codigo+".jpg")+'" height="35px" style="vertical-align:top;" /></td>';
		str += '<th style="vertical-align:middle;"><a href="##modal#product#'+product.id+'#open">'+product.nome+'</a></th>';
		str += '<td style="vertical-align:middle;"><small>'+product.tipo_codigo+':'+product.codigo+'</small></td>';
		str += "</tr>";
	}
	str += '</tbody>';
	str += '</table>';

	modal.find(".modal-body").html(str);

	modal.modal("show");
	sp.loading.hide();
};

sp.product.select = function(options) {
	if(sp.db.products.list.length == 0) {
		toastr.error("Não existem produtos para escolher");
		return false;
	}
	var options = options || {};
	options.selected = options.selected || new Array();
	options.multiple = options.multiple || false;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	options.confirm = options.confirm || function(){};

	$(".sp-product-select").remove();
	var modal_id = "sp-product-select-"+sp.product.last_id++;
	var modal = $('\
	<div class="modal sp-product-select" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<h4 class="modal-title" id="'+modal_id+'-label">Seleccionar Produtos</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer sp-product-select-btns">\
					<button type="button" class="btn btn-primary sp-confirm"><i class="fa fa-check"></i> Confirmar</button>\
					<button type="button" class="btn btn-default sp-cancel"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false,keyboard:false,backdrop:'static'});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);

	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});

	var str = '';
	str += '	<div style="padding: 5px 10px;background: #eee;">';
	str += '		<div class="btn-group btn-group-justified sp-product-select-btns">';
	if(options.multiple === true) {
		str += '		<a class="btn btn-default sp-select-all"><i class="fa fa-check-square-o"></i> Todos</a>';
		str += '		<a class="btn btn-default sp-select-none"><i class="fa fa-square-o"></i> Nenhum</a>';
	}
	str += '		</div>';
	str += '	</div>';
	str += '	<div class="sp-product-select-search">';
	str += '		<div class="input-group">';
	str += '			<span class="input-group-addon"><i class="fa fa-search"></i></span>';
	str += '			<input type="search" class="form-control" placeholder="Pesquisar produto">';
	str += '			<span class="input-group-addon btn btn-default"><i class="fa fa-times"></i></span>';
	str += '		</div>';
	str += '	</div>';
	str += '	<div style="padding: 10px;background: #eee;">';
	str += '		<div class="sp-product-select-name-filter">';
	str += '			<table style="margin:0;">';
	str += '				<tr>';
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	str += '					<td nowrap>*</td>';
	str += '					<td nowrap>0-9</td>';
	for (var i=0;i<possible.length;i++)
	{
		str += '				<td class="'+(i==0?"active":"")+'">'+possible[i]+'</td>';
	}
	str += '				</tr>';
	str += '			</table>';
	str += '		</div>';
	str += '	</div>';
	str += '	<div class="sp-product-select-list">';
	str += '		<table class="table table-striped table-condensed" style="margin:0;">';
	str += '			<tbody>';
	/*
	var last_initial = sp.db.products.list[0].nome[0].toUpperCase();;
	str += '				<tr style="'+(last_initial!="A"?"display:none":"")+'" class="sp-separator sp-initial-'+last_initial+'"><th colspan="20">'+last_initial+'</th></tr>';
	for ( var i in sp.db.products.list ) {
		var product = sp.db.products.list[i];
		if(last_initial!=product.nome[0]) {
			last_initial = product.nome[0].toUpperCase();
			str += '		<tr style="'+(last_initial!="A"?"display:none":"")+'" class="sp-separator sp-initial-'+last_initial+'"><th colspan="20">'+last_initial+'</th></tr>';
		}
		str += '			<tr style="'+(last_initial!="A"?"display:none":"")+'" data-id="'+product.id+'" class="sp-initial-'+last_initial+' '+((options.selected).indexOf(""+product.id)!=-1?"selected":"")+'">';
		str += '				<td class="sp-select-cell">';
		str += '					<img src="images/products/'+product.id+'.jpg" height="35px" />';
		str += '				</td>';
		str += '				<td class="sp-select-cell">';
		str += '					<span>'+product.nome+'<br><small class="text-muted">'+product.tipo_codigo+':'+product.codigo+'</small></span>';
		str += '				</td>';
		str += '				<td class="sp-select-cell"><span><i class="fa"></i></span></td>';
		str += "			</tr>";
	}
	*/
	str += '			</tbody>';
	str += '		</table>';
	str += '		<div class="alert alert-info" style="margin:10px;display:none"><i class="fa fa-info-circle"></i> Não foram encontrados produtos</div>';
	str += '	</div>';

	modal.find(".modal-body").html(str);

	var sp_products_select_search_input_timeout;
	modal.find(".modal-body .sp-product-select-search input").on("keyup",function(){
		if($(this).val()=="") {
			modal.find(".modal-body .sp-product-select-search .btn").attr("disabled","disabled");
			modal.find(".modal-body .sp-product-select-name-filter td.active").removeClass("active");
			modal.find(".modal-body .sp-product-select-name-filter td:eq(2)").addClass("active");
		}
		else {
			modal.find(".modal-body .sp-product-select-search .btn").removeAttr("disabled");
			modal.find(".modal-body .sp-product-select-name-filter td.active").removeClass("active");
			modal.find(".modal-body .sp-product-select-name-filter td:first").addClass("active");
		}

		clearTimeout(sp_products_select_search_input_timeout);
		sp_products_select_search_input_timeout = setTimeout(function(){
			modal.find(".modal-body .sp-product-select-list").trigger("update-content");
		},500);
	});
	modal.find(".modal-body .sp-product-select-search .btn").on("click",function(){
		modal.find(".modal-body .sp-product-select-search input").val("").trigger("keyup");
	});

	modal.find(".modal-body .sp-product-select-list").on("update-content",function(){
		var num_visible = 10;
		var letter = modal.find(".modal-body .sp-product-select-name-filter td.active").html().toUpperCase();
		if(letter=="*")letter="";
		var products = sp.db.products.filter({
			initial: letter,
			all: ""+modal.find(".modal-body .sp-product-select-search input").val(),
			nome: true,
			codigo: true
		});
		modal.find(".modal-body .sp-product-select-list tbody").empty();
		if(products.length == 0) {modal.find(".modal-body .sp-product-select-list .alert").show();return false;}
		var last_initial = products[0].nome[0].toUpperCase();
		str = '';
		str += '				<tr class="sp-separator sp-initial-'+last_initial+'"><th colspan="20">'+last_initial+'</th></tr>';
		var counter = 0;
		for ( var i in products ) {
			var product = products[i];
			if(last_initial!=product.nome[0].toUpperCase()) {
				last_initial = product.nome[0].toUpperCase();
				str += '		<tr class="sp-separator sp-initial-'+last_initial+'"><th colspan="20">'+last_initial+'</th></tr>';
			}
			str += '			<tr data-id="'+product.id+'" class="'+(counter<num_visible?'visible ':'')+'sp-initial-'+last_initial+' '+((options.selected).indexOf(""+product.id)!=-1?"selected":"")+'">';
			str += '				<td class="sp-select-cell">';
			str += '					<img src="'+sp.db.images.get(product.codigo+".jpg")+'" height="35px" />';
			str += '				</td>';
			str += '				<td class="sp-select-cell">';
			str += '					<span>'+product.nome+'<br><small class="text-muted">'+product.tipo_codigo+':'+product.codigo+'</small></span>';
			str += '				</td>';
			str += '				<td class="sp-select-cell"><span><i class="fa"></i></span></td>';
			str += "			</tr>";
			counter++;
		}

		if(counter>num_visible)
			str += '<tr class="sp-product-select-more"><td colspan="20"><a class="btn btn-success btn-block">Apresentar mais ...</a></td></tr>';

		modal.find(".modal-body .sp-product-select-list tbody").html(str);

		if(products.length==0) modal.find(".modal-body .sp-product-select-list .alert").show();
		else modal.find(".modal-body .sp-product-select-list .alert").hide();

		modal.find(".modal-body .sp-select-cell").on("click",function(){
			if($(this).parent().hasClass("selected")) {
				$(this).parent().removeClass("selected");
				options.selected.splice($.inArray($(this).parent().attr("data-id"), options.selected),1);
			}
			else {
				if(options.multiple === false) {
					modal.find(".modal-body .sp-product-select-list table tr.selected").removeClass("selected");
					options.selected.splice($.inArray(modal.find(".modal-body .sp-product-select-list table tr.selected").attr("data-id"), options.selected),1);
				}
				$(this).parent().addClass("selected");
				options.selected.push($(this).parent().attr("data-id"));
			}

		});

		modal.find(".modal-body .sp-product-select-more a").off("click").on("click",function(){
			if($(".sp-product-select-list tr").length==0)return;
			$(".sp-product-select-list tr:not(.visible):lt("+num_visible+")").addClass("visible");
			if($(".sp-product-select-list tr:not(.visible)").length==0)
				$(this).hide();
		});
	});
	modal.find(".modal-body .sp-select-all").on("click",function(){
		modal.find(".modal-body .sp-product-select-list table tr:not(.selected)").addClass("selected");
		options.selected = sp.db.client.list;
	});
	modal.find(".modal-body .sp-select-none").on("click",function(){
		modal.find(".modal-body .sp-product-select-list table tr.selected").removeClass("selected");
		options.selected = new Array();
	});
	modal.find(".modal-body .sp-product-select-name-filter td").on("click",function(){
		modal.find(".modal-body .sp-product-select-name-filter td.active").removeClass("active");
		$(this).addClass("active");
		modal.find(".modal-body .sp-product-select-list").trigger("update-content");
	});
	modal.find(".modal-footer .sp-confirm").on("click",function(){
		options.confirm(options.selected);
		modal.modal("hide");
	});
	modal.find(".modal-footer .sp-cancel").on("click",function(){
		modal.modal("hide");
	});

	modal.find(".modal-body .sp-product-select-list").trigger("update-content");

	modal.modal("show");
};

sp.debug.register("end");