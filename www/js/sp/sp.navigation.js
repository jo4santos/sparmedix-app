//var	History = window.History, // Note: We are using a capital H instead of a lower h
//State = History.getState();

sp.debug.register("start");

sp.navigation = {page: null};
sp.navigation.init = function () {
	
};
sp.navigation.hasAccess = function(page){
	for(var i in sp.user.navigation) {
		if(sp.user.navigation[i].url=="#"+page) return true;
	}
	return false;
};
sp.navigation.load = function(force) {
	var force = force || false;
	if(window.location.hash == "" && sp.terminal.mobile) {
		if(sp.navigation.exit) {
			if(navigator.Backbutton) {
				navigator.Backbutton.goHome(function() {
					//toastr.success('success home')
				}, function() {
					//toastr.error('fail home')
				});
			}
		}
		else {
			toastr.options.timeOut = 3000;
			toastr.info("Pressione novamente para sair");
			toastr.options.timeOut = 10000;
			sp.navigation.exit = true;
			setTimeout("sp.navigation.exit=false;",3000);
		}
	}

	if((window.location.hash=="#" || window.location.hash=="") && !force)
	{
		window.location.hash="#dashboard";
		return false;
	}
	var hash = window.location.hash;
	hash = hash.substr(1);
	
	if(hash[0]=="#")
	{
		if(sp.navigation.page==null) hash = "dashboard"+hash;
		else hash = sp.navigation.page+hash;
	}
	
	var fields = hash.split("#");
	
	if(sp.navigation.page != fields[0] || force)
	{
		sp.navigation.page = fields[0];
		if(!sp.navigation.hasAccess(sp.navigation.page)) {
			toastr.error('<span class="toastr-label">Acesso negado</span><br>Não tem permissões para aceder à página pedida')
			window.location.hash="#dashboard";
			return false;
		}
		sp.loading.show({text: "A carregar conteúdos ...", overlay: true});
		$(".sp-update-target").remove();
		$("#page-wrapper").after('<div style="display:none;" class="sp-update-target"></div>');
		$("#page-wrapper").removeClass("loaded");
		$("#page-wrapper").load( fields[0]+".html", function(responseText, textStatus, req) {
			$("#page-wrapper").addClass("loaded");
			if (textStatus == "error") {
				$("#page-wrapper").html('<div class="alert alert-danger">Erro a carregar a página, por favor tente novamente!</div>');
			}
			$(".side-nav .navbar-nav li.active").removeClass("active");
			$(".side-nav .navbar-nav a[href='#"+sp.navigation.page+"']").parent().addClass("active");
			$("*[data-toggle=tooltip]").tooltip();
			if(sp.terminal.mobile){
				$(".selectpicker").selectpicker('mobile');
			}
			else {
				$(".selectpicker").selectpicker();
			}
			$(".sp-no-follow").off("click").on("click",function(e){e.preventDefault();})
			sp.order.update();
			$("*[data-sp-allow]").each(function(){if($(this).attr("data-sp-allow").indexOf(sp.user.tipo)==-1) {$(this).remove();}});
			sp.loading.hide();

			var pending_notes = new Array();
			if(localStorage["notes.pending."+sp.user.id])
				pending_notes = JSON.parse(localStorage["notes.pending."+sp.user.id]);
			if(pending_notes.length>0) {
				$(".sp-dynamic-notes-sync").show();
				$(".sp-dynamic-notes-sync .badge").html(pending_notes.length);
			}
			else {
				$(".sp-dynamic-notes-sync").hide();
			}
		});
	}
	//$(".modal:visible").modal("hide");
	switch (fields[1]) {
		case "modal": {
			switch (fields[2]) {
			case "user": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				var user = sp.db.users.get_details(fields[3]);
				if(!user) {
					toastr.error("Utilizador não encontrado.");
					window.location.hash="##";
					return false;
				}
				$("title").html("Detalhes utilizador - "+user.nome);
				$(".modal:visible").modal("hide");
				sp.users.modal({
					id : fields[3]
				});
				break;
			}
			case "user-add": {
				if(fields[3] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]);
				}
				if(sp.user.tipo!=sp.db.users.types.admin) {
					toastr.error("Não tem permissão para aceder a este conteúdo");
					window.location.hash="##";
					return false;
				}
				$("title").html("Adicionar utilizador");
				$(".modal:visible").modal("hide");
				sp.users.add();
				break;
			}
			case "user-edit": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				var user = sp.db.users.get_details(fields[3]);
				if(!user) {
					toastr.error("Não tem permissão para aceder a este conteúdo");
					window.location.hash="##";
					return false;
				}
				$(".modal:visible").modal("hide");
				$("title").html("Editar utilizador - "+user.nome);
				sp.users.edit({
					id : fields[3]
				});
				break;
			}
			case "group-add": {
				if(fields[3] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]);
				}
				if(sp.user.tipo!=sp.db.users.types.admin) {
					toastr.error("Não tem permissão para aceder a este conteúdo");
					window.location.hash="##";
					return false;
				}
				$("title").html("Adicionar grupo");
				$(".modal:visible").modal("hide");
				sp.groups.add();
				break;
			}
			case "group-edit": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				var group = sp.db.groups.get_details(fields[3]);
				if(!group) {
					toastr.error("Não tem permissão para aceder a este conteúdo");
					window.location.hash="##";
					return false;
				}
				$(".modal:visible").modal("hide");
				$("title").html("Editar grupo - "+group.nome);
				sp.groups.edit({
					id : fields[3]
				});
				break;
			}
			case "group-clients": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				if(!sp.db.groups.get_details(fields[3])) {
					toastr.error("Não tem permissão para aceder a este conteúdo");
					window.location.hash="##";
					return false;
				}
				$(".modal:visible").modal("hide");
				var group = sp.db.groups.get_details(fields[3]);
				sp.client.list(group.clientes);
				break;
			}
			case "group-products": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				if(!sp.db.groups.get_details(fields[3])) {
					toastr.error("Não tem permissão para aceder a este conteúdo");
					window.location.hash="##";
					return false;
				}
				$(".modal:visible").modal("hide");
				var group = sp.db.groups.get_details(fields[3]);
				sp.product.list(group.produtos);
				break;
			}
			case "client": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				var client = sp.db.client.get_details(fields[3]);
				if(!client) {
					toastr.error("Cliente não encontrado.");
					window.location.hash="##";
					return false;
				}
				$(".modal:visible").modal("hide");
				$("title").html("Detalhes cliente - "+client.nome);
				sp.client.modal({
					id : fields[3]
				});
				$("*[data-sp-allow]").each(function(){if($(this).attr("data-sp-allow").indexOf(sp.user.tipo)==-1) {$(this).remove();}});
				break;
			}
			case "note": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				var note = sp.db.notes.get_details(fields[3]);
				if(!note) {
					toastr.error("Nota não encontrada.");
					window.location.hash="##";
					return false;
				}
				$(".modal:visible").modal("hide");
				$("title").html("Detalhes nota - "+note.codigo);
				sp.notes.modal({
					id : fields[3]
				});
				break;
			}
			case "mb": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				var note = sp.db.notes.get_details(fields[3]);
				if(!note) {
					toastr.error("Nota não encontrada.");
					window.location.hash="#notes";
					return false;
				}
				$("title").html("Dados pagamento - "+note.codigo);
				if(note.payee_id == note.client_id) {
					sp.notes.modal.mb({
						id : fields[3]
					});
				} else {
					window.location.hash="#notes";
					return false;
				}
				break;
			}
			case "product": {
				if(fields[4] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]+"#"+fields[3]);
				}
				var produto = sp.db.products.get_details(fields[3]);
				if(!produto) {
					toastr.error("Produto não encontrado.");
					window.location.hash="##";
					return false;
				}
				$("title").html("Detalhes produto - "+produto.nome);
				$(".modal:visible").modal("hide");
				sp.product.add({
					id: fields[3]
				});
				break;
			}
			case "order": {
				if(fields[3] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]);
				}
				$(".modal:visible").modal("hide");
				$("title").html("Detalhes da encomenda");

				$("#shopping-cart-modal").on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
				$("#shopping-cart-modal").modal("show");
				break;
			}
			case "configuration": {
				if(sp.terminal.mobile && !sp.terminal.connection) {
					toastr.info("Para alterar configurações necessita de ter ligação à internet.");
					window.location = "##";
					return;
				}
				if(fields[3] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]);
				}
				$("title").html("Configuração");
				$(".modal:visible").modal("hide");
				sp.configuration.modal();
				break;
			}
			case "profile": {
				if(sp.terminal.mobile && !sp.terminal.connection) {
					toastr.info("Para alterar o seu perfil necessita de ter ligação à internet.");
					window.location = "##";
					return;
				}
				if(fields[3] == "open")
				{
					//History.replaceState(null, "", "#"+fields[0]+"#"+fields[1]+"#"+fields[2]);
				}
				$("title").html("Editar perfil");
				$(".modal:visible").modal("hide");
				sp.profile.modal();
				break;
			}
			case "debug": {
				$("title").html("Consola debug");
				$(".modal:visible").modal("hide");
				$("#sp-debug-modal").modal("show");
				break;
			}
			}
			break;
		}
		default: {
			$(".modal:visible").modal("hide");
			break;
		}
	}
	
	/*localStorage["page-counter"]++;
	alert(localStorage["page-counter"]);*/
	
	$(".sp-checkbox:not(.sp-initialized)").each(function(){
		if($(this).find("input").length==0)
			$(this).prepend("<input type='checkbox' />");
		$(this).find("input").after("<span><i class='fa'></i></span>");
		$(this).on("click",function(){
			$(this).find("input").prop("checked", !$(this).find("input").prop("checked"));
		});
		$(this).addClass("sp-initialized");
	});
	$(".sp-product-image-popup").css("cursor","pointer").off("click").on("click",function(){
		var image = $(this).attr("data-image");
		str = '';
		str = '<div style="padding:50px;background:#ddd;text-align:center;"><img src="'+$(this).attr("src")+'" height="200px" width="200px" style="background:#ffffff;" /></div>';
		bootbox.dialog({
			message: str,
			title: image
		});
	});
};
$(".user-navigation-trigger").on("click",function(e){
	e.preventDefault();
	$(".user-navigation-trigger").toggleClass("active");
	$("#user-navigation").toggle();
	$("#user-navigation").toggleClass("active");
	if($("#user-navigation").is(":visible"))
	{
		$( "html" ).one( "click", function() {
			$( "html" ).one( "click", function(e) {
				if(e.target.className == "user-navigation-trigger") return;
				$(".user-navigation-trigger").removeClass("active");
				$("#user-navigation").hide();
				$("#user-navigation").removeClass("active");
			});
		});
	}
});
$(".navbar-sp-notifications").on("show.bs.dropdown",function(){
	$(".navbar-sp-notifications").addClass("active");
	//$("body").append("<div class='notifications-backdrop' style='position:fixed;top:0;bottom:0;left:0;right:0;opacity:.8;background:#000000;display: block;'></div>");
});
$(".navbar-sp-notifications").on("hide.bs.dropdown",function(){
	$(".navbar-sp-notifications").removeClass("active");
	//$(".notifications-backdrop").remove();
});
$(window).on('hashchange', function() {
	sp.navigation.load();
});

sp.debug.register("end");