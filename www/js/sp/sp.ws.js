sp.debug.register("start");

sp.ws = {
	url: "/ws/"
};
sp.ws.get = function (options) {
	options = options || {};
	options.url = options.url || "";
	options.data = options.data || {};
	options.callbacks = options.callbacks || function(){};
	options.callbacks["error"] 	= options.callbacks["error"] 	|| function(data, status, jqXHR){console.error("Generic error "+data.status+" fetching data: url "+sp.ws_url+"/ws/"+options.url+" | message "+data.responseText);};
	options.callbacks["403"] 	= options.callbacks["403"] 		|| function(data, status, jqXHR){
		sp.utils.storage.remove_auth(function(){
			sp.utils.login_message({message:"Sem sessão iniciada. Faça login para aceder."});
			window.location = "login.html";
		});
	};
	options.callbacks["401"] 	= options.callbacks["401"] 		|| function(data, status, jqXHR){
		sp.utils.storage.remove_auth(function(){
			sp.utils.login_message({message:"Iniciou sessao noutro terminal. Faça novamente login para aceder."});
			window.location = "login.html";
		});
	};
	options.callbacks["404"] 	= options.callbacks["404"] 		|| function(data, status, jqXHR){
		sp.utils.storage.remove_auth(function(){
			sp.utils.login_message({message:"Sem ligação ao servidor ("+sp.ws_url+"). Verifique a sua ligação à internet."});
			window.location = "login.html";
		});
	};
	options.callbacks["419"] 	= options.callbacks["419"] 		|| function(data, status, jqXHR){
		sp.utils.storage.remove_auth(function(){
			sp.utils.login_message({message:"Sessão iniciada expirou. Faça novamente login para aceder."});
			window.location = "login.html";
		});
	};
	options.data.auth_token = "";
	if(window.localStorage.getItem("auth-token"))
		options.data.auth_token = window.localStorage.getItem("auth-token");
	var statusCodes = {};
	for (var i in options.callbacks) {
		if(i!=="error") {
			options.callbacks[i] = options.callbacks[i] || function(data, status, jqXHR){console.log("Successfully got data from "+sp.ws_url+"/ws/"+options.url);};
			statusCodes[i] = options.callbacks[i];
		}
	}
	return $.ajax({
		type: "GET",
		url: sp.ws_url+"/ws/"+options.url,
		dataType: "json",
		data: options.data,
		statusCode: statusCodes,
		error: options.callbacks["error"]
	});
};
sp.ws.delete = function (options) {
	options = options || {};
	options.data = options.data || {};
	options.data.method = "delete";
	return sp.ws.post(options);
	
	
//	options = options || {};
//	options.url = options.url || "";
//	options.data = options.data || {};
//	options.callbacks = options.callbacks || function(){};
//	options.callbacks["error"] 	= options.callbacks["error"] 	|| function(data, status, jqXHR){console.error("Generic error "+data.status+" fetching data: url "+sp.ws_url+"/ws/"+options.url+" | message "+data.responseText);};
//	var statusCodes = {};
//	for (var i in options.callbacks) {
//		if(i!=="error") {
//			options.callbacks[i] = options.callbacks[i] || function(data, status, jqXHR){console.log("Successfully got data from "+sp.ws_url+"/ws/"+options.url);};
//			statusCodes[i] = options.callbacks[i];
//		}
//	}
//	return $.ajax({
//		type: "DELETE",
//		url: sp.ws_url+"/ws/"+options.url,
//		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//		dataType: "json",
//		data: options.data,
//		statusCode: statusCodes,
//		error: options.callbacks["error"]
//	});
};
sp.ws.put = function (options) {
	options = options || {};
	options.data = options.data || {};
	options.data.method = "put";
	return sp.ws.post(options);
	
	
//	options = options || {};
//	options.url = options.url || "";
//	options.data = options.data || {};
//	options.callbacks = options.callbacks || function(){};
//	options.callbacks["error"] 	= options.callbacks["error"] 	|| function(data, status, jqXHR){console.error("Generic error "+data.status+" fetching data: url "+sp.ws_url+"/ws/"+options.url+" | message "+data.responseText);};
//	var statusCodes = {};
//	for (var i in options.callbacks) {
//		if(i!=="error") {
//			options.callbacks[i] = options.callbacks[i] || function(data, status, jqXHR){console.log("Successfully got data from "+sp.ws_url+"/ws/"+options.url);};
//			statusCodes[i] = options.callbacks[i];
//		}
//	}
//	return $.ajax({
//		type: "PUT",
//		url: sp.ws_url+"/ws/"+options.url,
//		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//		dataType: "json",
//		data: options.data,
//		statusCode: statusCodes,
//		error: options.callbacks["error"]
//	});
};
sp.ws.post = function (options) {
	options = options || {};
	options.url = options.url || "";
	options.data = options.data || {};
	options.callbacks = options.callbacks || function(){};
	options.callbacks["error"] 	= options.callbacks["error"] 	|| function(data, status, jqXHR){console.error("Generic error "+data.status+" fetching data: url "+sp.ws_url+"/ws/"+options.url+" | message "+data.responseText);};

	options.data.auth_token = "";
	if(window.localStorage.getItem("auth-token"))
		options.data.auth_token = window.localStorage.getItem("auth-token");

	var statusCodes = {};
	for (var i in options.callbacks) {
		if(i!=="error") {
			options.callbacks[i] = options.callbacks[i] || function(data, status, jqXHR){console.log("Successfully got data from "+sp.ws_url+"/ws/"+options.url);};
			statusCodes[i] = options.callbacks[i];
		}
	}
	return $.ajax({
		type: "POST",
		url: sp.ws_url+"/ws/"+options.url,
		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		dataType: "json",
		data: options.data,
		statusCode: statusCodes,
		error: options.callbacks["error"]
	});
};

sp.debug.register("end");