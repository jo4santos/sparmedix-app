sp.debug.register("start");

sp.profile = {};
sp.profile.modal = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};

	$(".sp-profile-modal").remove();
	sp.loading.show({text:"A obter configurações"});
	var modal_id = "sp-profile-modal-"+sp.users.last_id++;
	var modal = $('\
	<div class="modal sp-profile-modal" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header" style="border-bottom:0;">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Editar perfil</h4>\
				</div>\
				<div class="modal-body" style="padding:0;"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer" style="border-top:0;">\
					<button type="button" class="btn btn-primary sp-profile-form-confirm"><i class="fa fa-check"></i> Guardar</button>\
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var user = sp.db.users.get_details(sp.user.id);

	var str = '';
	str += '<table id="sp-profile-configs" class="table table-striped table-bordered" style="margin:0;">';

	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;width:90px;"><label class="control-label"><b>Tipo</b></label></th>';
	str += '		<td colspan="2">';
	if(user.tipo == sp.db.users.types.admin) {
		str += '			<span class="label label-warning">Administrador</span>';
	}
	else if(user.tipo == sp.db.users.types.sales) {
		str += '			<span class="label label-primary">Vendedor</span>';
	}
	else if(user.tipo == sp.db.users.types.client) {
		str += '			<span class="label label-success">Cliente</span>';
	}
	str += '		</td>';
	str += '		<td class="text-center" style="width:50px;"><i class="fa fa-asterisk text-info"></i></td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label class="control-label"><b>Grupo</b></label></th>';
	str += '		<td colspan="2">';
	str += '			'+(user.grupo?'<span class="label label-default">'+sp.db.groups.get_details(user.grupo).nome+'</span>':'-');
	str += '		</td>';
	str += '		<td class="text-center"><i class="fa fa-asterisk text-info"></i></td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label class="control-label"><b>E-mail</b></label></th>';
	if(user.tipo == sp.db.users.types.client) {
		str += '		<td colspan="2">'+user.email+'</td>';
		str += '		<td class="text-center"><i class="fa fa-asterisk text-info"></i></td>';
	}
	else {
		str += '		<td style="vertical-align:middle;" colspan="2">'+user.email+'</td>';
		str += '		<td style="display:none;" colspan="2"><input type="email" id="sp-profile-email-input" class="form-control" placeholder="E-mail" value="'+user.email+'"></td>';
		str += '		<td class="text-center"><button class="btn btn-default" id="sp-profile-email-toggle"><i class="fa fa-pencil"></i></button></td>';		
	}
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label class="control-label"><b>Nome</b></label></th>';
	if(user.tipo == sp.db.users.types.client) {
		str += '		<td colspan="2">'+user.nome+'</td>';
		str += '		<td class="text-center"><i class="fa fa-asterisk text-info"></i></td>';
	}
	else {
		str += '		<td style="vertical-align:middle;" colspan="2">'+user.nome+'</td>';
		str += '		<td style="display:none;" colspan="2"><input type="text" id="sp-profile-nome-input" class="form-control" placeholder="Nome" value="'+user.nome+'"></td>';
		str += '		<td class="text-center"><button class="btn btn-default" id="sp-profile-nome-toggle"><i class="fa fa-pencil"></i></button></td>';		
	}
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label class="control-label"><b>Password</b></label></th>';
	str += '		<td colspan="2" style="vertical-align:middle;"><span style="font-size:20px;">&bullet;&bullet;&bullet;&bullet;&bullet;&bullet;</span></td>';
	str += '		<td style="display:none;"><input type="password" id="sp-profile-password-input" class="form-control" placeholder="Nova Password"></td>';
	str += '		<td style="display:none;"><input type="password" id="sp-profile-password-confirm-input" class="form-control" placeholder="Confirmar"></td>';
	str += '		<td class="text-center"><button class="btn btn-default" id="sp-profile-password-toggle"><i class="fa fa-pencil"></i></button></td>';
	str += '	</tr>';
	str += '	<tr><td colspan="5" class="alert alert-info"><i class="fa fa-asterisk text-info"></i> <small>Para editar os campos sinalizados, por favor contacte um administrador</small></td></tr>';

	str += '</table>';
	modal.find(".modal-body").html(str);

	modal.find("#sp-profile-password-toggle").on("click",function(){
		$(this).find("i").toggleClass("fa-pencil").toggleClass("fa-times");
		$(this).closest("tr").toggleClass("edited").find("td:eq(0),td:eq(1),td:eq(2)").toggle();
		modal.find(".modal-footer").trigger("update");
	});

	modal.find("#sp-profile-nome-toggle,#sp-profile-email-toggle").on("click",function(){
		$(this).find("i").toggleClass("fa-pencil").toggleClass("fa-times");
		$(this).closest("tr").toggleClass("edited").find("td:eq(0),td:eq(1)").toggle();
		modal.find(".modal-footer").trigger("update");
	});

	modal.find(".modal-footer").hide().on("update",function() {
		if(modal.find("#sp-profile-configs tr.edited").length == 0) {
			$(this).hide();
		} else {
			$(this).show();
		}
	});

	modal.find(".sp-profile-form-confirm").on("click",function() {
		var user = sp.db.users.get_details(sp.user.id);
		var nome = modal.find("#sp-profile-nome-input");
		var email = modal.find("#sp-profile-email-input");
		var password = modal.find("#sp-profile-password-input");
		var password_confirm = modal.find("#sp-profile-password-confirm-input");

		modal.find(".modal-body .has-error").removeClass("has-error");
		modal.find(".modal-body .error-message").remove();

		var configs = new Array();

		var error_counter = 0;
		if(nome.closest("tr").hasClass("edited") && nome.val()!=user.nome) {
			if(nome.val()==="") {
				nome.closest(".form-group").addClass("has-error");
				nome.after('<small class="error-message text-danger">Campo obrigatório</small>')
				error_counter++;
			}
			else {
				configs.push({"name":"nome","value":nome.val()});
			}
		}
		if(email.closest("tr").hasClass("edited") && email.val()!=user.email) {
			if(email.val()==="") {
				email.closest(".form-group").addClass("has-error");
				email.after('<small class="error-message text-danger">Campo obrigatório</small>')
				error_counter++;
			}
			else if(!sp.utils.validate.email(email.val())) {
				email.closest(".form-group").addClass("has-error");
				email.after('<small class="error-message text-danger">E-mail inválido</small>')
				error_counter++;
			}
			else {
				configs.push({"name":"email","value":email.val()});
			}
		}
		if(password.closest("tr").hasClass("edited")) {
			if(password.val()!=password_confirm.val()) {
				password_confirm.closest(".form-group").addClass("has-error");
				password_confirm.after('<small class="error-message text-danger">Confirme a password</small>')
				error_counter++;
			}
			else if(password.val()=="") {
				password.closest(".form-group").addClass("has-error");
				password.after('<small class="error-message text-danger">Campo obrigatório</small>')
				error_counter++;
			}
			else if(password.val().length<6) {
				password.closest(".form-group").addClass("has-error");
				password.after('<small class="error-message text-danger">Mínimo de 6 caracteres</small>')
				error_counter++;
			}
		}
		if(error_counter>0) return false;
		sp.loading.show({text: "A aplicar alterações", overlay: true});
		if(password.closest("tr").hasClass("edited")) {
			sp.utils.auth.crypt({p:password.val(),s:"h1s2a3h4s5i6d7e8m9raps",d:function(hash){
				configs.push({"name":"password","value":hash});
				sp.db.user.update_profile(configs,sp.user.id,function(){
					sp.loading.hide();
					modal.modal("hide");
				});
			}});
		} else {
			sp.db.user.update_profile(configs,sp.user.id,function(){
				sp.loading.hide();
				modal.modal("hide");
			});
		}
	});

	modal.modal("show");
	sp.loading.hide();
};

sp.debug.register("end");