
/*html5sql.process("CREATE TABLE IF NOT EXISTS spdb (id INTEGER PRIMARY KEY, data TEXT)",
	function(){

		var data = "jose";

		html5sql.process("DELETE FROM spdb WHERE id='2';",
		     function(transaction, results){
				html5sql.process("INSERT INTO spdb (id,data) VALUES ('2','"+data+"');",
					function(transaction, results){
				         console.error(transaction);
				         console.error(results);
					},
					function(error, statement){
						console.error("Error: " + error.message + " when processing " + statement);
					}
				 );
		     },
		     function(error, statement){
		         console.error("Error: " + error.message + " when processing " + statement);
		     }        
		 );

		// GET AND PROCESS DATA

		
     },
     function(error, statement){
         console.error("Error: " + error.message + " when processing " + statement);
     }        
 );*/



sp.utils.storage = {};
	sp.utils.storage.init = function() {
		html5sql.openDatabase("com.sparmedix.orders","Sparmedix DB",50*1024*1024);
		html5sql.process("CREATE TABLE IF NOT EXISTS spdb (key TEXT PRIMARY KEY, value TEXT)",
			function(transaction, results){},
			function(error, statement){
				console.error("Error: " + error.message + " when processing " + statement);
			}
		);
		if(sp) {
			if(sp.user) {
				html5sql.process("CREATE TABLE IF NOT EXISTS spdb_"+sp.user.id+" (key TEXT PRIMARY KEY, value TEXT)",
					function(transaction, results){},
					function(error, statement){
						console.error("Error: " + error.message + " when processing " + statement);
					}
				);
			}
		}
	};
	sp.utils.storage.init();
	sp.utils.storage.get = function(key,callback) {
		var callback = callback || function(){};
			html5sql.process(["SELECT value FROM spdb_"+sp.user.id+" WHERE key='"+key+"';"],
				function(transaction, results, rows){
					if(rows.length>0)
						callback(JSON.parse(unescape(rows[0].value)));
				},
				function(error, statement){
					console.error("Error: " + error.message + " when processing " + statement);
				});
	};
	sp.utils.storage.set = function(key,value,id) {
		var id =  id || sp.user.id;
		if(!sp.terminal.mobile && false)
		{
			var storage = new Array();
			if(localStorage[sp.utils.md5("spdb")])	storage = JSON.parse(sp.utils.dec(localStorage[sp.utils.md5("spdb")]));
			var found = false;
			for (var i in storage[i]) {
				if(storage[i]["u"] == id) {
					found = true;
					break;
				}
			}
			if(!found) {
				var o = {"u":id,"v":[]};
				storage.push(o);
			}
			for (var i in storage) {
				if(storage[i]["u"] == id) {
					for (var j in storage[i]["v"]) {
						if(storage[i]["v"][j]["k"]==key) {
							storage[i]["v"][j]["v"] = value;
							localStorage[sp.utils.md5("spdb")] = sp.utils.enc(JSON.stringify(storage));
							return;
						}
					}
					var o = {"k":key,"v":value};
					storage[i]["v"].push(o);
					localStorage[sp.utils.md5("spdb")] = sp.utils.enc(JSON.stringify(storage));
					return;
				}
			}
		}
		else {
			html5sql.process(["DELETE FROM spdb_"+sp.user.id+" WHERE key='"+key+"';","INSERT INTO spdb_"+sp.user.id+" (key,value) VALUES ('"+key+"','"+escape(JSON.stringify(value))+"');"],
				function(transaction, results){
					//console.error(transaction);
					//console.error(results);
				},
				function(error, statement){
					console.error("Error: " + error.message + " when processing " + statement);
				});
		}
	};
	sp.utils.storage.remove_auth = function(callback) {
		var callback = callback || function(){};
		html5sql.process(["DELETE FROM spdb where key='auth-token';"],function(transaction, results){callback();});
	}
	if(window.location.href.indexOf("login.html")!=-1 ) {
		sp.utils.storage.remove_auth();
	}