sp.debug.register("start");
var sp = sp || {};
sp.db = {};
sp.db.update = function () {
	/*if(sp.terminal.mobile && !sp.terminal.connection) {
		toastr.error('Não existe ligação ao servidor. A base de dados local será atualizada quando ligar novamente.');
		return false;
	}*/
	$.when(
		sp.db.groups.update_list(), 
		sp.db.users.update_list(), 
		sp.db.client.update_list(), 
		sp.db.products.update_list(), 
		sp.db.notes.update_list(),
		sp.db.notifications.update_list()
	).then(function(){
		$(window).trigger("sp-updated-db");
		
	});	

	return true;
};
sp.db.timeout_tick = function(refresh) {
	var refresh = refresh || false;

	if(!sp.user) return false;

	var time_since_login = Math.round(+new Date()/1000)-sp.user.session_start;
	var time_to_expire = 60*60*24*7 - time_since_login;
	var time_to_expire_days   = Math.floor(Math.floor(time_to_expire / 3600) / 24);
	var time_to_expire_hours   = Math.floor(time_to_expire / 3600)-time_to_expire_days*24;
    var time_to_expire_minutes = Math.floor((time_to_expire - (Math.floor(time_to_expire / 3600) * 3600)) / 60);
    var time_to_expire_seconds = time_to_expire - (Math.floor(time_to_expire / 3600) * 3600) - (time_to_expire_minutes * 60);
    var time_str = "";
    if(time_to_expire_days>0) time_str+=time_to_expire_days+"d ";
    if(time_to_expire_hours>0) time_str+=sp.utils.zerofill(time_to_expire_hours,2)+"h ";
    if((time_to_expire_minutes>0 && time_to_expire_hours==0) || time_to_expire_hours>0) time_str+=sp.utils.zerofill(time_to_expire_minutes,2)+"m ";
    time_str+=sp.utils.zerofill(time_to_expire_seconds,2)+"s";
    if(time_to_expire_hours>0 || time_to_expire_minutes>0 || time_to_expire_seconds>0) {
		$(".sp-status-expiration .label").removeClass("label-default label-danger").addClass("label-default").html(time_str);
    }
    else {
    	$(".sp-status-expiration .label").removeClass("label-default label-danger").addClass("label-danger").html("terminada");
    }
	
	if(sp.terminal.mobile && !sp.terminal.connection) {
		sp.db.o_timeout = setTimeout("sp.db.timeout_tick()",1000);
		$(".sp-dynamic-db-sync-timeout").html(" - ");
		$(".sp-update-trigger i").hide();
		return false;
	}

	$(".sp-update-trigger i").show();
	if(sp.db.timeout==null) sp.db.timeout = 600;
	if(sp.db.o_timeout==null) sp.db.o_timeout = null;
	$(".sp-dynamic-db-sync-timeout").html(sp.db.timeout);
	if(sp.db.timeout--<=0) {
		clearInterval(sp.db.o_timeout);
		$(".sp-dynamic-db-sync-timeout").html("");
		$(".sp-update-trigger i").addClass("fa-spin");
		$(window).off("sp-updated-db").on("sp-updated-db", function(){
			sp.db.timeout = 600;
			$(".sp-dynamic-db-sync-timeout").html(sp.db.timeout);

			//var timeout = toastr.options.timeOut;
			//toastr.options.timeOut = 1000;
			//toastr.info("Base de dados local foi atualizada");
			//toastr.options.timeOut = timeout;

			$(".sp-update-trigger i").removeClass("fa-spin");
			$(".sp-update-trigger").removeClass("disabled");
			$(".sp-update-target").trigger("sp-update-content");

			sp.db.notifications.update_list()

			var user = sp.db.users.get_details(sp.user.id);

			sp.user.nome = user.nome;
			sp.user.email = user.email;
			$(".sp-user-dynamic-nome").html(sp.user.nome);
			clearInterval(sp.db.o_timeout);
			sp.db.timeout_tick();
		});
		sp.db.images.check();
		if(!sp.db.update())
		{
			toastr.error('Erro ao inicializar a base de dados');
		}
	}
	else
	{
		clearInterval(sp.db.o_timeout);
		sp.db.o_timeout = setTimeout("sp.db.timeout_tick()",1000);
	}
};
$(".sp-update-trigger").on("click",function(){
	if($(".sp-update-trigger").hasClass("disabled")) return;
	$(".sp-update-trigger").addClass("disabled");
	clearInterval(sp.db.o_timeout);
	sp.db.timeout = 0;
	sp.db.timeout_tick(true);
});

sp.db.user = {};
sp.db.user.update_fav_clients = function(clients,id) {
	id = id || sp.user.id;
	if(!$.isArray(clients)) {
		console.error("[sp][sp.db.user.set_fav_clients] clients of wrong type");
		return false;
	}
	return sp.ws.put({
		url: "users/clients",
		data: {
			id: id,
			clients: clients
		},
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.user.fav_clients = clients;
				var timeout = toastr.options.timeOut;
				toastr.options.timeOut = 3000;
				toastr.success("Lista de clientes favoritos atualizada");
				toastr.options.timeOut = timeout;
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao atualizar clientes favoritos: "+data.responseText);
			}
		}
	});	
};
sp.db.user.update_fav_products = function(products,id) {
	id = id || sp.user.id;
	if(!$.isArray(products)) {
		console.error("[sp][sp.db.user.update_fav_products] products of wrong type");
		return false;
	}
	return sp.ws.put({
		url: "users/products",
		data: {
			id: id,
			products: products
		},
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.user.fav_products = products;
				var timeout = toastr.options.timeOut;
				toastr.options.timeOut = 3000;
				toastr.success("Lista de produtos favoritos atualizada");
				toastr.options.timeOut = timeout;
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao atualizar produtos favoritos: "+data.responseText);
			}
		}
	});	
};
sp.db.user.update_configs = function(configs,id) {
	id = id || sp.user.id;
	if(!$.isArray(configs)) {
		console.error("[sp][sp.db.user.update_configs] configs of wrong type");
		return false;
	}
	if(sp.terminal.mobile && !sp.terminal.connection) {
		toastr.danger("Para alterar configurações é necessária ligação à internet");
		return false;		
	}
	sp.loading.show({overlay: true, text: "A aplicar alterações ..."});
	return sp.ws.put({
		url: "users/configs",
		data: {
			id: id,
			configs: configs
		},
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.loading.hide();
				sp.user.configs = data;
				var timeout = toastr.options.timeOut;
				toastr.options.timeOut = 3000;
				toastr.success("Configurações de utilizador atualizadas");
				toastr.options.timeOut = timeout;

				$.when(sp.db.notes.update_list()).done(function(notes){
							$(".sp-update-target").trigger("sp-update-content");
							sp.loading.hide();
						});

				sp.loading.hide();
				$(".sp-update-trigger").trigger("click");
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao atualizar configurações: "+data.responseText);
				sp.loading.hide();
			}
		}
	});	
};
sp.db.user.update_profile = function(configs,id,done) {
	id = id || sp.user.id;
	done = done || function(){};
	if(!$.isArray(configs)) {
		console.error("[sp][sp.db.user.update_profile] configs of wrong type");
		return false;
	}
	if(sp.terminal.mobile && !sp.terminal.connection) {
		toastr.danger("Para alterar perfil é necessária ligação à internet");
		return false;		
	}
	sp.loading.show({overlay: true, text: "A aplicar alterações ..."});
	return sp.ws.put({
		url: "users/profile",
		data: {
			id: id,
			configs: configs
		},
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.loading.hide();
				sp.user.configs = data;
				var timeout = toastr.options.timeOut;
				toastr.options.timeOut = 3000;
				toastr.success("Perfil de utilizador atualizado");
				toastr.options.timeOut = timeout;
				$(".sp-update-trigger").trigger("click");
				done();
			},
			"error": function (data, status, jqXHR)  {
				sp.loading.hide();
				toastr.error("Erro ao atualizar perfil: "+data.responseText);
				done();
			}
		}
	});	
};

sp.db.client = {
	list: new Array()
};
sp.db.client.get_details = function(id) {
	for (var i in sp.db.client.list)
	{
		if(sp.db.client.list[i].id == id)
		{
			return sp.db.client.list[i];
		}
	}
	return false;
};
sp.db.client.update_list = function () {
	if(sp.terminal.mobile && !sp.terminal.connection) {
		sp.utils.storage.get("client.list",function(variable){
			sp.db.client.list = variable;
		});
		return true;
	}
	return sp.ws.get({
		url: "clients",
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.db.client.list = new Array();
				for ( var i in data )
					sp.db.client.list.push({id:data[i].id,codigo:data[i].codigo,nome:data[i].nome,nif:data[i].nif,responsavel:data[i].responsavel,morada:data[i].morada,email:data[i].email,local:data[i].local,cdpostal:data[i].cdpostal,tppreco:data[i].tppreco});
				sp.utils.storage.set("client.list",sp.db.client.list);
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao obter lista de clientes: "+data.responseText);
			}
		}
	});
};
sp.db.client.filter = function(options) {
	var options = options || {};
	options.all = options.all || false;
	options.nome = options.nome || false;
	options.nif = options.nif || false;
	options.codigo = options.codigo || false;
	options.responsavel = options.responsavel || false;
	options.morada = options.morada || false;
	options.email = options.email || false;
	options.initial = options.initial || false;
	
	options.favorites = options.favorites || false;	
	var clients = sp.db.client.list;
	
	if(options.favorites)
		clients = $.grep(clients, function(v) {return $.inArray( v.id, sp.user.fav_clients ) != -1;});
	
	if(typeof options.all === 'string') {
		clients = $.grep(clients, function(v) {
			return 	(v.nome.toUpperCase().indexOf			(options.all.toUpperCase()) != -1 && options.nome == true) || 
					(v.nif.toUpperCase().indexOf			(options.all.toUpperCase()) != -1 && options.nif == true) || 
					(v.codigo.toUpperCase().indexOf		(options.all.toUpperCase()) != -1 && options.codigo == true) || 
					(v.responsavel.toUpperCase().indexOf	(options.all.toUpperCase()) != -1 && options.responsavel == true) || 
					((v.morada+v.local+v.cdpostal).toUpperCase().indexOf		(options.all.toUpperCase()) != -1 && options.morada == true) || 
					(v.email.toUpperCase().indexOf		(options.all.toUpperCase()) != -1 && options.email == true);
		});
	}
	else {
		if(typeof options.nome === 'string') {
			clients = $.grep(clients, function(v) {
				return v.nome.toUpperCase().indexOf(options.nome.toUpperCase()) != -1;
			});
		}
		if(typeof options.nif === 'string') {
			clients = $.grep(clients, function(v) {
				return v.nif.toUpperCase().indexOf(options.nif.toUpperCase()) != -1;
			});
		}
	}
	if(options.initial!==false) {
		if(options.initial == "0-9") {
			clients = $.grep(clients, function(v) {
				return 	v.nome.charAt(0).toUpperCase() == "0" ||
						v.nome.charAt(0).toUpperCase() == "1" ||
						v.nome.charAt(0).toUpperCase() == "2" ||
						v.nome.charAt(0).toUpperCase() == "3" ||
						v.nome.charAt(0).toUpperCase() == "4" ||
						v.nome.charAt(0).toUpperCase() == "5" ||
						v.nome.charAt(0).toUpperCase() == "6" ||
						v.nome.charAt(0).toUpperCase() == "7" ||
						v.nome.charAt(0).toUpperCase() == "8" ||
						v.nome.charAt(0).toUpperCase() == "9";
			});
		} else {
			clients = $.grep(clients, function(v) {
				return v.nome.charAt(0).toUpperCase() == options.initial.toUpperCase();
			});				
		}
	}
	return clients;
};

sp.db.notes = {
	list: new Array(),
	products: new Array(),
	last_id: 1,
	products_last_id: 1
};
sp.db.notes.get_by_client_id = function(id) {
	var notes = new Array();
	for (var i in sp.db.notes.list)
	{
		if(sp.db.notes.list[i].client_id == id)
		{
			notes.push(sp.db.notes.list[i]);
		}
	}
	return notes;
};
sp.db.notes.get_by_user_id = function(id) {
	var notes = new Array();
	for (var i in sp.db.notes.list)
	{
		if(sp.db.notes.list[i].user_id == id)
		{
			notes.push(sp.db.notes.list[i]);
		}
	}
	return notes;
};
sp.db.notes.get_details = function(id) {
	for (var i in sp.db.notes.list)
	{
		if(sp.db.notes.list[i].id == id)
		{
			return sp.db.notes.list[i];
		}
	}
	return false;
};
sp.db.notes.get_products = function(id) {
	var products = new Array();
	for (var i in sp.db.notes.products)
	{
		if(sp.db.notes.products[i].note_id == id)
		{
			products.push(sp.db.notes.products[i]);
		}
	}
	return products;
};
sp.db.notes.update_list = function () {
	if(sp.terminal.mobile && !sp.terminal.connection) {
		sp.utils.storage.get("notes.list",function(variable){
			sp.db.notes.list = variable;
		});
		sp.utils.storage.get("notes.products",function(variable){
			sp.db.notes.products = variable;
		});
		return true;
	}
	return sp.ws.get({
		url: "notes",
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.db.notes.list = new Array();
				sp.db.notes.products = new Array();
				for ( var i in data ) {
					if(parseInt(data[i].id)>parseInt(sp.db.notes.last_id)) sp.db.notes.last_id = parseInt(data[i].id);
					sp.db.notes.list.push({
						id:data[i].id,
						payee_id:data[i].payee_id,
						client_id:data[i].client_id,
						user_id:data[i].user_id,
						codigo:data[i].codigo,
						data:data[i].data,
						referencia:data[i].referencia,
						estado:data[i].estado,
                        preco:sp.utils.format_price_val(data[i].preco),
						client_nome:data[i].client_nome,
						payee_nome:data[i].payee_nome,
						user_nome:data[i].user_nome,
						data_payment:data[i].data_payment,
						reject_user_id:data[i].reject_user_id,
						reject_user_name:data[i].reject_user_name,
						reject_data:data[i].reject_data,
						close_user_id:data[i].close_user_id,
						close_user_name:data[i].close_user_name,
						close_data:data[i].close_data
					});
					for ( var j in data[i].produtos ) {
						sp.db.notes.products.push({id:data[i].produtos[j].id,note_id:data[i].produtos[j].note_id,product_id:data[i].produtos[j].product_id,product_nome:data[i].produtos[j].product_nome,quantidade:data[i].produtos[j].quantidade,bonus:data[i].produtos[j].bonus,discount:data[i].produtos[j].discount,preco:sp.utils.format_price_val(data[i].produtos[j].preco),iva:data[i].produtos[j].iva,codigo:data[i].produtos[j].codigo});
						if(parseInt(data[i].produtos[j].id)>parseInt(sp.db.notes.products_last_id)) sp.db.notes.products_last_id = parseInt(data[i].produtos[j].id);
					}
				}

				//sp.db.notes.list.unshift(note);
				//for ( var j in note.products ) {
				//	sp.db.notes.products.push({id:note.products[j].id,note_id:note.id,product_id:note.products[j].product_id,product_nome:note.products[j].nome,quantidade:note.products[j].quantidade,bonus:note.products[j].bonus,preco:sp.utils.format_price_val(note.products[j].preco),iva:note.products[j].iva});
				//}
		
				var pending_notes = new Array();
				if(localStorage["notes.pending."+sp.user.id])
					pending_notes = JSON.parse(localStorage["notes.pending."+sp.user.id]);

				for ( var i in pending_notes ) {
					var note = pending_notes[i];

					sp.db.notes.last_id++;
					note.id = sp.db.notes.last_id;
					sp.db.notes.list.unshift(note);
					for ( var j in note.products ) {
						sp.db.notes.products.push({id:sp.db.notes.products_last_id++,note_id:note.id,product_id:note.products[j].id,product_nome:note.products[j].nome,quantidade:note.products[j].quantidade,bonus:note.products[j].bonus,discount:note.products[j].discount,preco:sp.utils.format_price_val(note.products[j].preco),iva:note.products[j].iva,codigo:note.products[j].codigo});
					}
				}
				sp.utils.storage.set("notes.list",sp.db.notes.list);
				sp.utils.storage.set("notes.products",sp.db.notes.products);				
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao obter lista de notas: "+data.responseText);
			}
		}
	});
};
sp.db.notes.create = function () {

	var client_id = sp.order.client;
	var products = sp.order.list;
	
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	var possiblecodes = ['EAN','CNP'];
	var notes = new Array();
	sp.loading.show({overlay: true, text: "A inserir nota ..."});
	var note = {};
	if(sp.order.comment) {
		note.comment = sp.order.comment;
	}
	note.client_id=sp.order.client;
	var client_details = sp.db.client.get_details(note.client_id);
	note.client_nome=client_details.nome;
	note.payee_id=sp.order.bill_client || sp.order.client;
	var client_details = sp.db.client.get_details(note.payee_id);
	note.payee_nome=client_details.nome;
	note.user_id=sp.user.id;
	var user_details = sp.db.users.get_details(note.user_id);
	note.user_nome=user_details.nome;
	note.referencia=sp.order.referencia;
	note.preco=sp.order.valor;
	note.data=sp.utils.format_date(Math.round(+new Date()/1000));
	//note.codigo=note.client_id+""+note.user_id+""+note.data;
	var year = new Date().getFullYear();
	sp.db.notes.last_id=0;
	for ( var i in sp.db.notes.list ) {
		if(parseInt(sp.db.notes.list[i].id)>parseInt(sp.db.notes.last_id) && sp.db.notes.list[i].estado!=0) sp.db.notes.last_id = parseInt(sp.db.notes.list[i].id);
	}
	sp.db.notes.last_id++;
	note.codigo = sp.utils.get_initials(note.user_nome)+sp.utils.zerofill(note.user_id,3)+year+sp.utils.zerofill(sp.db.notes.last_id,4);
	note.estado=1;
	if(sp.order.referencia=="")
		note.estado=4;

	note.products=new Array();
	for ( var i in sp.order.list ) {
		var product = sp.order.list[i];
		var product_details = sp.db.products.get_details(product.id);
		product.preco = sp.db.products.get_price(product.id);
		product.iva = product_details.iva;
		product.quantidade = product.quantity;
		product.nome = product_details.nome;
		product.codigo = product_details.codigo;
		note.products.push(product);
	}


	notes.push(note);

	if(!sp.terminal.connection && sp.terminal.mobile) {
		note.estado=0;
		var pending_notes = new Array();
		if(localStorage["notes.pending."+sp.user.id])
			pending_notes = JSON.parse(localStorage["notes.pending."+sp.user.id]);

		sp.db.notes.last_id=0;
		for(var i in sp.db.notes.list) {
			if(parseInt(sp.db.notes.list[i].id)>parseInt(sp.db.notes.last_id)) sp.db.notes.last_id = parseInt(sp.db.notes.list[i].id);
		}
		note.codigo = sp.utils.get_initials(note.user_nome)+sp.utils.zerofill(note.user_id,3)+year+sp.utils.zerofill(sp.db.notes.last_id,4)+"T";
		sp.db.notes.last_id++;
		note.codigo = "Pendente "+parseInt(parseInt(pending_notes.length)+1);
		note.id = sp.db.notes.last_id;
		pending_notes.unshift(note);
		localStorage["notes.pending."+sp.user.id] = JSON.stringify(pending_notes);

		sp.db.notes.list.unshift(note);
		for ( var j in note.products ) {
			sp.db.notes.products_last_id++;
			sp.db.notes.products.push({id:sp.db.notes.products_last_id,note_id:note.id,product_id:note.products[j].id,product_nome:note.products[j].nome,quantidade:note.products[j].quantidade,bonus:note.products[j].bonus,discount:note.products[j].discount,preco:sp.utils.format_price_val(note.products[j].preco),iva:note.products[j].iva});
		}

		sp.utils.storage.set("notes.list",sp.db.notes.list);
		sp.utils.storage.set("notes.products",sp.db.notes.products);

		toastr.success("Encomenda gravada localmente. Será enviada quando existir ligação ao servidor.");
		sp.order.client = null;
		sp.order.bill_client = null;
		sp.order.list = new Array();
		sp.order.update();
		sp.loading.hide();
		window.location="#notes";
		return;
	}

	sp.ws.put({
		url:"notes",
		data:{
			"notes":notes
		},
		callbacks:{
			"200":function(data,status,jqXHR){
				toastr.success("Encomenda inserida com sucesso");
				$.when(sp.db.notes.update_list()).done(function(){
					var show_mb = true;
					if(sp.order.bill_client!=null && sp.order.client != sp.order.bill_client) {
						show_mb=false;
					}
					
					sp.order.client = null;
					sp.order.bill_client = null;
					sp.order.list = new Array();
					sp.order.update();
					sp.loading.hide();
					if(show_mb) {
						window.location="#notes#modal#mb#"+data[0].id+"#open";
					} else {
						window.location="#notes#modal#"+data[0].id+"#open";
					}
				});
			},
			"error":function(data,status,jqXHR){
				toastr.error("Erro ao inserir nota de encomenda");
				sp.loading.hide();
			}
		}
	});
	
	//alert(sp.order.client)
};
sp.db.notes.reject = function (id) {

	sp.loading.show({overlay: true, text: "A cancelar nota ..."});
	var note = sp.db.notes.get_details(id);
	if(note.estado == "0") {
		var pending_notes = new Array();
		if(localStorage["notes.pending."+sp.user.id])
			pending_notes = JSON.parse(localStorage["notes.pending."+sp.user.id]);
		var new_notes = new Array();
		for(var i in pending_notes) {
			if(pending_notes[i].codigo == note.codigo) continue;
			new_notes.push(pending_notes[i]);
		}
		localStorage["notes.pending."+sp.user.id] = JSON.stringify(new_notes);

		sp.db.notes.last_id=0;
		for ( var i in sp.db.notes.list ) {
			if(parseInt(sp.db.notes.list[i].id) == parseInt(id)) {
				sp.db.notes.list.splice(i,1);
				break;
			}
		}
		sp.loading.hide();
		$(".sp-update-target").trigger("sp-update-content");
		toastr.success("Encomenda removida com sucesso");
		window.location="##";
		return;
	}

	sp.ws.put({
		url:"notes/reject",
		data:{
			"id":id
		},
		callbacks:{
			"200":function(data,status,jqXHR){
				toastr.success("Encomenda cancelada com sucesso");
				window.location="##";
				$(".sp-update-trigger").trigger("click");
				sp.loading.hide();
			},
			"error":function(data,status,jqXHR){
				toastr.error("Erro ao cancelar nota de encomenda");
				sp.loading.hide();
			}
		}
	});
	
	//alert(sp.order.client)
};
sp.db.notes.close = function (id) {

	sp.loading.show({overlay: true, text: "A fechar nota ..."});
	sp.ws.put({
		url:"notes/close",
		data:{
			"id":id
		},
		callbacks:{
			"200":function(data,status,jqXHR){
				toastr.success("Encomenda fechada com sucesso");
				window.location="##";
				$(".sp-update-trigger").trigger("click");
				sp.loading.hide();
			},
			"error":function(data,status,jqXHR){
				toastr.error("Erro ao fechar encomenda");
				sp.loading.hide();
			}
		}
	});
	
	//alert(sp.order.client)
};
sp.db.notes.filter = function(options) {
	var options = options || {};
	options.all = options.all || false;
	options.numero = options.numero || false;
	options.cliente = options.cliente || false;
	options.vendedor = options.vendedor || false;

	var notes = sp.db.notes.list;
	
	if(typeof options.all === 'string') {
		notes = $.grep(notes, function(v) {
			return 	(v.codigo.toUpperCase().indexOf			(options.all.toUpperCase()) != -1 && options.numero == true) || 
					(v.client_nome.toUpperCase().indexOf	(options.all.toUpperCase()) != -1 && options.cliente == true) || 
					(v.user_nome.toUpperCase().indexOf		(options.all.toUpperCase()) != -1 && options.vendedor == true);
		});
	}
	else {
		if(typeof options.numero === 'string') {
			notes = $.grep(notes, function(v) {
				return v.codigo.toUpperCase().indexOf(options.numero.toUpperCase()) != -1;
			});
		}
		if(typeof options.cliente === 'string') {
			notes = $.grep(notes, function(v) {
				return v.client_nome.toUpperCase().indexOf(options.cliente.toUpperCase()) != -1;
			});
		}
		if(typeof options.vendedor === 'string') {
			notes = $.grep(notes, function(v) {
				return v.user_nome.toUpperCase().indexOf(options.vendedor.toUpperCase()) != -1;
			});
		}
	}
	return notes;
};

sp.db.products = {
	list: new Array()
};
sp.db.products.get_price = function (productId, formatted) {
    var product = sp.db.products.get_details(productId);
    var preco = product.preco;
    if(sp.order.client) {
        var client = sp.db.client.get_details(sp.order.client);
        if(client.tppreco == 1) {
            preco = product.pvalsiva;
        }
        else if(client.tppreco == 2) {
            preco = product.pvp3siva;
        }
    }
    if(formatted) {
        preco = sp.utils.format_price(preco);
    }
    else {
        preco = parseFloat(preco);
    }
    return preco;
}
sp.db.products.get_details = function(id) {
	for (var i in sp.db.products.list)
	{
		if(sp.db.products.list[i].id == id)
		{
			return sp.db.products.list[i];
		}
	}
	return false;
};
sp.db.products.update_list = function () {
	if(sp.terminal.mobile && !sp.terminal.connection) {
		sp.utils.storage.get("products.list",function(variable){
			sp.db.products.list = variable;
		});
		return true;
	}
	return sp.ws.get({
		url: "products",
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.db.products.list = new Array();
				for ( var i in data )
					sp.db.products.list.push({id:data[i].id,tipo_codigo:data[i].tipo_codigo,codigo:data[i].codigo,nome:data[i].nome,preco:sp.utils.format_price_val(data[i].preco),pvalsiva:sp.utils.format_price_val(data[i].pvalsiva),pvp3siva:sp.utils.format_price_val(data[i].pvp3siva),bonus:data[i].bonus,iva:data[i].iva,familia:data[i].familia});
				sp.utils.storage.set("products.list",sp.db.products.list);
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao obter lista de produtos: "+data.responseText);
			}
		}
	});
};
sp.db.products.filter = function(options) {
	var options = options || {};
	options.all = options.all || false;
	options.nome = options.nome || false;
	options.codigo = options.codigo || false;
	options.familias = options.familias || false;
	options.initial = options.initial || false;
	
	options.favorites = options.favorites || false;	
	var products = sp.db.products.list;
	
	if(options.favorites)
		products = $.grep(products, function(v) {return $.inArray( v.id, sp.user.fav_products ) != -1;});
	if(options.familias)
		products = $.grep(products, function(v) {return $.inArray( v.familia, options.familias ) != -1;});
	
	if(typeof options.all === 'string') {
		products = $.grep(products, function(v) {
			return 	(v.nome.toUpperCase().indexOf			(options.all.toUpperCase()) != -1 && options.nome == true) || 
					(v.codigo.toUpperCase().indexOf		(options.all.toUpperCase()) != -1 && options.codigo == true);
		});
	}
	else {
		if(typeof options.nome === 'string') {
			products = $.grep(products, function(v) {
				return v.nome.toUpperCase().indexOf(options.nome.toUpperCase()) != -1;
			});
		}
		if(typeof options.codigo === 'string') {
			products = $.grep(products, function(v) {
				return v.codigo.toUpperCase().indexOf(options.codigo.toUpperCase()) != -1;
			});
		}
	}
	if(options.initial!==false) {
		if(options.initial == "0-9") {
			products = $.grep(products, function(v) {
				return 	v.nome.charAt(0).toUpperCase() == "0" ||
						v.nome.charAt(0).toUpperCase() == "1" ||
						v.nome.charAt(0).toUpperCase() == "2" ||
						v.nome.charAt(0).toUpperCase() == "3" ||
						v.nome.charAt(0).toUpperCase() == "4" ||
						v.nome.charAt(0).toUpperCase() == "5" ||
						v.nome.charAt(0).toUpperCase() == "6" ||
						v.nome.charAt(0).toUpperCase() == "7" ||
						v.nome.charAt(0).toUpperCase() == "8" ||
						v.nome.charAt(0).toUpperCase() == "9";
			});
		} else {
			products = $.grep(products, function(v) {
				return v.nome.charAt(0).toUpperCase() == options.initial.toUpperCase();
			});				
		}
	}
	return products;
};

sp.db.users = {
	list: new Array(),
	types: {admin:0,sales:1,client:2}
};
sp.db.users.get_details = function(id) {
	for (var i in sp.db.users.list)
	{
		if(sp.db.users.list[i].id == id)
		{
			return sp.db.users.list[i];
		}
	}
	return false;
};
sp.db.users.update_list = function () {
	if(sp.terminal.mobile && !sp.terminal.connection) {
		sp.utils.storage.get("users.list",function(variable){
			sp.db.users.list = variable;
		});
		return true;
	}
	return sp.ws.get({
		url: "users",
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.db.users.list = new Array();
				for ( var i in data )
					sp.db.users.list.push({id:data[i].id,tipo:data[i].tipo,grupo:data[i].grupo,nome:data[i].nome,email:data[i].email,configs:data[i].configs,client_id:data[i].client_id});
				sp.utils.storage.set("users.list",sp.db.users.list);
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao obter lista de utilizadores: "+data.responseText);
			}
		}
	});
};

sp.db.groups = {
	list: new Array()
};
sp.db.groups.get_details = function(id) {
	for (var i in sp.db.groups.list)
	{
		if(sp.db.groups.list[i].id == id)
		{
			return sp.db.groups.list[i];
		}
	}
	return false;
};

sp.db.groups.update_list = function () {
	if(sp.terminal.mobile && !sp.terminal.connection) {
		sp.utils.storage.get("groups.list",function(variable){
			sp.db.groups.list = variable;
		});
		return true;
	}
	return sp.ws.get({
		url: "groups",
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.db.groups.list = new Array();
				for ( var i in data )
					sp.db.groups.list.push({id:data[i].id,nome:data[i].nome,tipo:data[i].tipo,descricao:data[i].descricao,lista_distribuicao:data[i].lista_distribuicao,clientes:data[i].clientes,produtos:data[i].produtos});
				sp.utils.storage.set("groups.list",sp.db.groups.list);
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao obter lista de grupos: "+data.responseText);
			}
		}
	});
};

sp.db.notifications = {
	list: new Array()
};
sp.db.notifications.get_details = function(id) {
	for (var i in sp.db.notifications.list)
	{
		if(sp.db.notifications.list[i].id == id)
		{
			return sp.db.notifications.list[i];
		}
	}
	return false;
};

sp.db.notifications.update_list = function () {
	if(sp.terminal.mobile && !sp.terminal.connection) {
		sp.utils.storage.get("notifications.list",function(variable){
			sp.db.notifications.list = variable;
			sp.notifications.update_content();
		});
		return true;
	}
	return sp.ws.get({
		url: "notifications",
		callbacks: {
			"200": function (data, status, jqXHR)  {
				sp.db.notifications.list = new Array();
				for ( var i in data )
					sp.db.notifications.list.push(data[i]);
				sp.utils.storage.set("notifications.list",sp.db.notifications.list);
				sp.notifications.update_content();
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao obter lista de notificacoes: "+data.responseText);
			}
		}
	});
};

sp.notifications = {};
sp.notifications.update_content = function() {
	$(".messages-dropdown .dropdown-menu li:not(.keep)").remove();
	var counter = 0;
	for ( var i in sp.db.notifications.list ) {
		var notification = sp.db.notifications.list[i];
		var element = $(".navbar-user .messages-dropdown .dropdown-menu .template").clone();
		element.removeClass("template");
		element.removeClass("keep");
		if(notification.type=="mb_payment" && notification.status=="1") {
			var fields = notification.comment.split("|");
			if (notification.note_id) {
				/*var note_code = fields[0];
				var entidade = fields[1];
				var referencia = fields[2];
				var valor = fields[3];
				var data = fields[4];*/
				element.find("a").attr("href","##modal#note#"+notification.note_id+"#open");
				element.find(".message").html("Pagamento recebido:"+fields[0]);
				element.find(".time").html(notification.data);
				element.find("i").addClass("fa-euro");
				element.find(".label").addClass("label-success");
				$(".navbar-user .messages-dropdown .dropdown-menu .divider").before(element);
				element.show();
				element = element.clone();
				$(".navbar-header .messages-dropdown .dropdown-menu .divider").before(element);
				element.show();
				counter++;
			}
		}
		else if(notification.type=="note_new" && notification.status=="1") {
			var fields = notification.comment.split("|");
			if (notification.note_id) {
				/*var note_code = fields[0];
				var entidade = fields[1];
				var referencia = fields[2];
				var valor = fields[3];
				var data = fields[4];*/
				element.find("a").attr("href","##modal#note#"+notification.note_id+"#open");
				element.find(".message").html("Nova encomenda: "+fields[0]);
				element.find(".time").html(notification.data);
				element.find("i").addClass("fa-shopping-cart");
				element.find(".label").addClass("label-primary");
				$(".navbar-user .messages-dropdown .dropdown-menu .divider").before(element);
				element.show();
				element = element.clone();
				$(".navbar-header .messages-dropdown .dropdown-menu .divider").before(element);
				element.show();
				counter++;
			}
		}
		else if(notification.type=="note_closed" && notification.status=="1") {
			var fields = notification.comment.split("|");
			if (notification.note_id) {
				/*var note_code = fields[0];
				var entidade = fields[1];
				var referencia = fields[2];
				var valor = fields[3];
				var data = fields[4];*/
				element.find("a").attr("href","##modal#note#"+notification.note_id+"#open");
				element.find(".message").html("Encomenda fechada: "+fields[0]);
				element.find(".time").html(notification.data);
				element.find("i").addClass("fa-check");
				element.find(".label").addClass("label-success");
				$(".navbar-user .messages-dropdown .dropdown-menu .divider").before(element);
				element.show();
				element = element.clone();
				$(".navbar-header .messages-dropdown .dropdown-menu .divider").before(element);
				element.show();
				counter++;
			}
		}
		else if(notification.type=="note_cancelled" && notification.status=="1") {
			var fields = notification.comment.split("|");
			if (notification.note_id) {
				/*var note_code = fields[0];
				var entidade = fields[1];
				var referencia = fields[2];
				var valor = fields[3];
				var data = fields[4];*/
				element.find("a").attr("href","##modal#note#"+notification.note_id+"#open");
				element.find(".message").html("Encomenda cancelada: "+fields[0]);
				element.find(".time").html(notification.data);
				element.find("i").addClass("fa-times");
				element.find(".label").addClass("label-danger");
				$(".navbar-user .messages-dropdown .dropdown-menu .divider").before(element);
				element.show();
				element = element.clone();
				$(".navbar-header .messages-dropdown .dropdown-menu .divider").before(element);
				element.show();
				counter++;
			}
		}
		if(counter==5) break;
	}
};

sp.db.images = {
	downloads:null,
	download_size:0,
	toastr:null,
	images:{},
	counter:0,
	later:null
};
sp.db.images.update_list = function () {
	if(sp.terminal.mobile && !sp.terminal.connection) {
		return true;
	}
	return sp.ws.get({
		url: "images",
		callbacks: {
			"200": function (data, status, jqXHR)  {
				Object.keys(localStorage).forEach(function(key){
           			if (/^(imagem-)/.test(key)) {
               			localStorage.removeItem(key);
           			}
       			});
				for ( var i in data ) {
					localStorage["imagem-"+i] = data[i];
				}
			},
			"error": function (data, status, jqXHR)  {
				toastr.error("Erro ao obter imagens: "+data.responseText);
			}
		}
	});
};
sp.db.images.get = function (name) {
	if(!sp.terminal.mobile) {
		return sp.ws_url+"/ws/images/"+name+"/display";
	}
	else {
		var l_images = {};
		if(localStorage["images"]) {
			l_images = JSON.parse(localStorage["images"]);
			if(l_images[name]) {
				return "file:///storage/emulated/0/com.sparmedix.orders/images/"+name;
			}
		}
		return "images/fallback.svg";
	}
};
sp.db.images.check = function () {
	if(!sp.terminal.mobile) return;
	if(sp.db.images.later!=null) {
		var diff = +new Date();
		diff -= sp.db.images.later;
		if(diff<3600000) return;
	}
	if(sp.db.images.toastr!=null) return;
	$("body").off("sp-sb-checked-fs-images").on("sp-sb-checked-fs-images",function(){
		var new_images = {};
		for(var i in sp.db.images.images) {
			if(sp.db.images.images[i].check == 1) {
				sp.db.images.images[i].check = 0;
				new_images[sp.db.images.images[i].name] = sp.db.images.images[i];
			}
		}
		localStorage["images"] = JSON.stringify(new_images);
		sp.ws.get({
			url: "images",
			callbacks: {
				"200": function (data, status, jqXHR)  {
					var r_images = data;
					var l_images = {};
					var downloads = {};
					var deletes = {};
					var download_size = 0;
					if(localStorage["images"]) {
						l_images = JSON.parse(localStorage["images"]);
					}
					for(var i in l_images) {
						if(!r_images[i]) {
							r_images[i] = l_images[i];
							r_images[i].action = "delete";
							deletes[i] = r_images[i];
						}
						else {
							sp.db.images.images[l_images[i].name] = l_images[i];
						}
					}
					for(var i in r_images) {
						if(!l_images[i]) {
							r_images[i].action = "download";
							download_size += parseInt(r_images[i].filesize);
							downloads[i] = r_images[i];
						}
						else if(r_images[i].filemtime != l_images[i].filemtime && r_images[i].filesize != l_images[i].filesize) {
							r_images[i].action = "download";
							download_size += parseInt(r_images[i].filesize);
							downloads[i] = r_images[i];
						}
						else {
							if(r_images[i].action != "delete")
								delete r_images[i];
						}
					}

					for(var i in deletes) {
						window.resolveLocalFileSystemURL("file:///storage/emulated/0/com.sparmedix.orders/images/"+deletes[i].name,
							function(fileEntry){
								fileEntry.remove(function(){},function(){});
							},function(){});
					}
					if(Object.keys(downloads).length==0) return;
					download_size = download_size/1024;
					download_size = download_size/1024;
					download_size = download_size.toFixed(2);
					var str ='';
					str += "Existe"+(Object.keys(downloads).length>1?"m":"")+" "+Object.keys(downloads).length+" image"+(Object.keys(downloads).length>1?"ns":"m")+" ("+download_size+"MB) para descarregar<br><br>";
					str += "<div class='btn-group btn-group-justified'>";
					str += "	<a class='btn btn-sm btn-primary' onclick='sp.db.images.update();'>Descarregar agora</a>";
					str += "	<a class='btn btn-sm btn-default' onclick='toastr.clear(sp.db.images.toastr);sp.db.images.toastr=null;sp.db.images.later=+new Date();' style='color:#000'>Mais tarde</a>";
					str += "</div>";
					var timeout = toastr.options.timeOut;
					var extendedTimeOut = toastr.options.extendedTimeOut;
					toastr.options.timeOut = 0;
					toastr.options.extendedTimeOut = 0;
					toastr.options.tapToDismiss = false;
					sp.db.images.toastr = toastr.info(str);
					toastr.options.timeOut = timeout;
					toastr.options.extendedTimeOut = extendedTimeOut;
					toastr.options.tapToDismiss = true;

					sp.db.images.downloads = downloads;
					sp.db.images.download_size = download_size;
				},
				"error": function (data, status, jqXHR)  {
					toastr.error("Erro ao obter imagens: "+data.responseText);
				}
			}
		});
	});
	sp.db.images.counter = 0;
	if(localStorage["images"]) {
		sp.db.images.images = JSON.parse(localStorage["images"]);
		if(Object.keys(sp.db.images.images).length == 0) {
			$("body").trigger("sp-sb-checked-fs-images");
		}
		else {
			for(var i in sp.db.images.images) {
				window.resolveLocalFileSystemURL("file:///storage/emulated/0/com.sparmedix.orders/images/"+sp.db.images.images[i].name,
					function(fileEntry){
						sp.db.images.images[fileEntry.name].check = 1;
						sp.db.images.counter++;
						if(sp.db.images.counter == Object.keys(sp.db.images.images).length) $("body").trigger("sp-sb-checked-fs-images");
					}, 
					function() {
						sp.db.images.counter++;
						if(sp.db.images.counter == Object.keys(sp.db.images.images).length) $("body").trigger("sp-sb-checked-fs-images");
					});
			}
		}
	}
	else {
		$("body").trigger("sp-sb-checked-fs-images");
	}
	return true;
};
sp.db.images.update = function() {
	toastr.clear(sp.db.images.toastr);
	bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Serão descarregados "+sp.db.images.download_size+"MB, confirma que pretende continuar?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						var str ='';
						var title = 'A descarregar '+Object.keys(sp.db.images.downloads).length+' image'+(Object.keys(sp.db.images.downloads).length>1?"ns":"m")+' ('+sp.db.images.download_size+'MB)';
						str = 'Estado: <span id="sp-db-images-download-total-counter">0</span>/'+Object.keys(sp.db.images.downloads).length+' completo<br>Erros: <span id="sp-db-images-download-error-counter">0</span><br><br>';
						str += '<div class="progress progress-striped active" style="margin:0;">';
					  	str += '<div id="sp-db-images-download-progress-bar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">';
					    str += '0%';
					  	str += '</div>';
						str += '</div>';
						var timeout = toastr.options.timeOut;
						var extendedTimeOut = toastr.options.extendedTimeOut;
						toastr.options.timeOut = 0;
						toastr.options.extendedTimeOut = 0;
						toastr.options.tapToDismiss = false;
						sp.db.images.toastr = toastr.info(str,title);
						toastr.options.timeOut = timeout;
						toastr.options.extendedTimeOut = extendedTimeOut;
						toastr.options.tapToDismiss = true;
						for (var i in sp.db.images.downloads) {
							if(sp.terminal.mobile) {
								var filename = sp.db.images.downloads[i].name;
								var filepath = "file:///storage/emulated/0/com.sparmedix.orders/images/"+filename;
								var fileTransfer = new FileTransfer();
								fileTransfer.download(
								    sp.ws_url+"/ws/images/"+filename,
								    filepath,
								    function(entry) {
								    	var filename = (entry.fullPath).replace(/^.*[\\\/]/, '');
										sp.db.images.images[filename] = sp.db.images.downloads[filename];
										var total_counter = parseInt($("#sp-db-images-download-total-counter").html());
										var error_counter = parseInt($("#sp-db-images-download-error-counter").html());
										total_counter++;
										var success_counter = total_counter - error_counter;
										var percentage = total_counter*100/Object.keys(sp.db.images.downloads).length;
										$("#sp-db-images-download-total-counter").html(total_counter);
										$('#sp-db-images-download-progress-bar').css('width', percentage+'%').attr('aria-valuenow', percentage).html(percentage+"%");
										if(total_counter==Object.keys(sp.db.images.downloads).length) {
											var str = '';
											str += '<p><i class="fa fa-check text-success" style="width:20px;display:inline-block;margin-left:10px;"></i> '+success_counter+' com sucesso</p>';
											str += '<p><i class="fa fa-times text-danger" style="width:20px;display:inline-block;margin-left:10px;"></i> '+error_counter+' com erro</p>';
											toastr.info(str,"Atualização de imagens de produtos terminada");
											toastr.clear(sp.db.images.toastr);
											localStorage["images"] = JSON.stringify(sp.db.images.images);
											sp.db.images.toastr = null;
										}
								    },
								    function(error) {
								        var total_counter = parseInt($("#sp-db-images-download-total-counter").html());
										var error_counter = parseInt($("#sp-db-images-download-error-counter").html());
										error_counter++;
										total_counter++;
										var success_counter = total_counter - error_counter;
										var percentage = total_counter*100/Object.keys(sp.db.images.downloads).length;
										$("#sp-db-images-download-total-counter").html(total_counter);
										$('#sp-db-images-download-progress-bar').css('width', percentage+'%').attr('aria-valuenow', percentage).html(percentage+"%");   
										$("#sp-db-images-download-error-counter").html(error_counter);
										if(total_counter==Object.keys(sp.db.images.downloads).length) {
											var str = '';
											str += '<p><i class="fa fa-check text-success" style="width:20px;display:inline-block;margin-left:10px;"></i> '+success_counter+' com sucesso</p>';
											str += '<p><i class="fa fa-times text-danger" style="width:20px;display:inline-block;margin-left:10px;"></i> '+error_counter+' com erro</p>';
											toastr.info(str,"Atualização de imagens de produtos terminada");
											toastr.clear(sp.db.images.toastr);
											localStorage["images"] = JSON.stringify(sp.db.images.images);
											sp.db.images.toastr = null;
										}
										//toastr.error("Erro no download:<br>Source: " + error.source+"<br>Target: " + error.target + "<br>Code" + error.code);
								    },
								    {
								        headers: {
								            "Connection": "close"
								        }
								    }		    
								);
							}
						}
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default",
					callback: function(){
						sp.db.images.toastr = null;
					}
				}
			}
		});
}

sp.debug.register("end");

