sp.debug.register("start");

sp.users = {last_id:0};
sp.users.modal = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	if(options.id==-1)
	{
		console.error("[sp][sp.users.modal()] invalid user id");
		return false;
	}
	sp.loading.show({text:"A obter informações do utilizador"});
	var modal_id = "sp-users-modal-"+sp.users.last_id++;
	var modal = $('\
	<div class="modal" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Detalhes do utilizador</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var user = sp.db.users.get_details(options.id);
	var notes = sp.db.notes.get_by_user_id(options.id);
	if(user.tipo == sp.db.users.types.client) {
		var notes = sp.db.notes.get_by_client_id(user.client_id);
	}
	var str = '';
	str += '<table class="table table-striped table-bordered">';
	str += '<tr><th><b>Nome</b></th><td>'+user.nome+'</td></tr>';
	if (user.tipo == sp.db.users.types.admin) {
		str += '<tr><th><b>Tipo</b></th><td><span class="label label-warning">Administrador</span></td></tr>';
	}
	else if(user.tipo == sp.db.users.types.sales) {
		str += '<tr><th><b>Tipo</b></th><td><span class="label label-primary">Vendedor</span></td></tr>';
	}
	else if(user.tipo == sp.db.users.types.client) {
		str += '<tr><th><b>Tipo</b></th><td><span class="label label-success">Cliente</span></td></tr>';
	}
	str += '<tr><th><b>Grupo</b></th><td>'+(user.grupo?'<span class="label label-default">'+sp.db.groups.get_details(user.grupo).nome+'</span>':'-')+'</td></tr>';
	str += '<tr><th><b>E-mail</b></th><td><a href="mailto:'+user.email+'">'+user.email+'</a></td></tr>';
	if(user.tipo == 2) {
		var client = sp.db.client.get_details(user.client_id);
		str += '<tr><th><b>Cliente associado</b></th><td>'+client.nome+'</td></tr>';
	}

	if(user.configs.email_notification.value == "true") {
		str += '<tr><th><b>Notificações por e-mail</b></th><td class="text-success"><i class="fa fa-check"></i> activo</td></tr>';
	} else {
		str += '<tr><th><b>Notificações por e-mail</b></th><td class="text-danger"><i class="fa fa-times"></i> inactivo</td></tr>';
	}

	str += '</table>';
	str += '<h5 class="sp-header"><i class="fa fa-shopping-cart"></i> Encomendas <small style="display:inline-block;margin-left:10px">('+notes.length+')</small></h5>';
	if(notes.length>0)
	{
		str += '<table class="table table-striped table-bordered sp-notes" style="margin-bottom:10px">';
		for(var i in notes)
		{
			var note = notes[i];
			str += '<tr class="sp-animate sp-slide">';
			str += '<td class="text-center" style="width:30px;vertical-align:middle;" nowrap>';
			if(note.estado == 0)
				str += '<span class="label label-default" data-toggle="tooltip" title="Pendente"><i class="fa fa-clock-o"></i></span>';
			else if(note.estado == 1)
				str += '<span class="label label-warning" data-toggle="tooltip" title="Em pagamento"><i class="fa fa-credit-card"></i></span>';
			else if(note.estado == 2)
				str += '<span class="label label-primary" data-toggle="tooltip" title="Em progresso"><i class="fa fa-truck"></i></span>';
			else if(note.estado == 3)
				str += '<span class="label label-danger" data-toggle="tooltip" title="Rejeitada"><i class="fa fa-times"></i></span>';
			else if(note.estado >= 4)
				str += '<span class="label label-success" data-toggle="tooltip" title="Tratada"><i class="fa fa-check"></i></span>';
			str += '</td>';
			str += '<td><a href="##modal#note#'+note.id+'#open">'+note.codigo+'</a><br><small style="display:block;line-height:1em;" class="text-muted">'+note.data+'</small></td>';
			str += '<td>'+note.client_nome+'</td>';
			str += '<td style="vertical-align:middle;text-align:center;width:100px;">';
			str += '<span class="label label-success" style="font-size:1em;display:block;">'+sp.utils.format_price(note.preco)+' &euro;</span>';
			str += '</td>';
			str += '</tr>';
		}
		str += '</table>';
		str += '<div class="modal-btn"><button class="btn btn-default sp-load-more"><i class="fa fa-plus"></i> Ver mais encomendas</button></div>';
	}
	else
	{
		str += '<div class="alert alert-info"><i class="fa fa-info-circle"></i> Sem encomendas registadas</div>';
	}

	modal.find(".modal-body").html(str);

	modal.find(".modal-body").find(".sp-load-more").on("click",function(){
		modal.find(".modal-body").find(".sp-notes tr:not(.visible):lt(5)").addClass("visible");
		if(modal.find(".modal-body").find(".sp-notes tr:not(.visible)").length==0) $(this).hide();
	}).trigger("click");
	
	$(".modal:visible").modal("hide");
	
	modal.modal("show");
	sp.loading.hide();
};

sp.users.add = function(options) {
	options = options || {};
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};

	$(".sp-user-add-dialog").remove();

	var modal_id = "sp-users-modal-"+sp.users.last_id++;
	var modal = $('\
	<div class="modal sp-user-add-dialog" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Adicionar novo utilizador</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer">\
					<button type="button" class="btn btn-primary sp-user-add-form-confirm"><i class="fa fa-check"></i> Confirmar</button>\
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var str = '';
	str += '<table class="table table-striped table-bordered sp-user-add-form">';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-user-add-form-tipo" class="control-label"><b>Tipo</b></label></th>';
	str += '		<td colspan="2">';
	str += '			<label style="margin-right:10px;"><input type="radio" name="sp-user-add-form-tipo" value="0" /> <span class="label label-warning">Administrador</span></label>';
	str += '			<label style="margin-right:10px;"><input type="radio" name="sp-user-add-form-tipo" value="1" /> <span class="label label-primary">Vendedor</span></label>';
	str += '			<label><input type="radio" name="sp-user-add-form-tipo" checked value="2" /> <span class="label label-success">Cliente</span></label>';
	str += '		</td>';
	str += '	</tr>';
	str += '	<tr class="form-group" id="sp-user-add-form-client">';
	str += '		<th style="vertical-align:middle;"><label class="control-label"><b>Cliente associado</b></label></th>';
	str += '		<td style="vertical-align:middle;" colspan="2"><button class="btn btn-default">Seleccionar</button> <span class="selection" data-id="">Nenhum cliente seleccionado</span></td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-user-add-form-nome" class="control-label"><b>Nome</b></label></th>';
	str += '		<td colspan="2"><input type="text" disabled="disabled" class="form-control" id="sp-user-add-form-nome" placeholder="Nome"></td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-user-add-form-email" class="control-label"><b>E-mail</b></label></th>';
	str += '		<td colspan="2"><input type="email" disabled="disabled" class="form-control" id="sp-user-add-form-email" placeholder="E-mail"></td>';
	str += '	</tr>';
	//str += '	<tr class="form-group">';
	//str += '		<th style="vertical-align:middle;"><label for="sp-user-add-form-password" class="control-label"><b>Password</b></label></th>';
	//str += '		<td><input type="password" class="form-control" id="sp-user-add-form-password" placeholder="Password"></td>';
	//str += '		<td><input type="password" class="form-control" id="sp-user-add-form-password-confirm" placeholder="Confirmar"></td>';
	//str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-user-add-form-tipo" class="control-label"><b>Grupo</b></label></th>';
	str += '		<td colspan="2">';
	str += '			<select class="form-control" id="sp-user-add-form-grupo">';
	str += '				<option value="">Seleccionar</option>';
	for (var i in sp.db.groups.list)
	{
		str += '				<option class="sp-group-user-type sp-group-user-type-'+sp.db.groups.list[i].tipo+'" value="'+sp.db.groups.list[i].id+'">'+sp.db.groups.list[i].nome+'</option>';
	}
	str += '			</select>';
	str += '		</td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<td class="sp-checkbox" style="vertical-align:middle;" colspan="3">';
	str += '			<input type="checkbox" id="sp-user-add-form-notifications" checked /> Enviar notificações de pagamentos por e-mail';
	str += '		</td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<td style="vertical-align:middle;" colspan="3">';
	str += '			<div class="alert alert-info"><i class="fa fa-info-circle"></i> A password será gerada automaticamente e enviada para o e-mail do utilizador.</div>';
	str += '		</td>';
	str += '	</tr>';
	str += '</table>';
	
	modal.find(".modal-body").html(str);
	
	modal.find(".modal-body input[name='sp-user-add-form-tipo']").change(function(){
		str = '';
		str += '				<option value="">Seleccionar</option>';
		for (var i in sp.db.groups.list)
		{
			if($(this).val()==sp.db.groups.list[i].tipo)
			str += '				<option class="sp-group-user-type sp-group-user-type-'+sp.db.groups.list[i].tipo+'" value="'+sp.db.groups.list[i].id+'">'+sp.db.groups.list[i].nome+'</option>';
		}
		if($(this).val()==sp.db.users.types.client) {
			$("#sp-user-add-form-client").show();
			$("#sp-user-add-form-nome").prop("disabled",true);
			$("#sp-user-add-form-email").prop("disabled",true);
		} else {
			$("#sp-user-add-form-client").hide();
			$("#sp-user-add-form-nome").prop("disabled",false);
			$("#sp-user-add-form-email").prop("disabled",false);
		}
		modal.find(".modal-body #sp-user-add-form-grupo").html(str);
	});
	modal.find(".modal-body #sp-user-add-form-client button").on("click",function(){
		var selected = [];
		if(modal.find(".modal-body #sp-user-add-form-client .selection").attr("data-id")!="")
			selected.push(modal.find(".modal-body #sp-user-add-form-client .selection").attr("data-id"));
		sp.client.select({
			selected: selected,
			multiple: false,
			confirm:function(selected){
				for(var i in sp.db.client.list) {
					if(sp.db.client.list[i].id == selected[0]) {
						modal.find(".modal-body #sp-user-add-form-client .selection").attr("data-id",selected[0]);
						modal.find(".modal-body #sp-user-add-form-client .selection").html(sp.db.client.list[i].nome);
						modal.find(".modal-body #sp-user-add-form-nome").val(sp.db.client.list[i].nome);
						modal.find(".modal-body #sp-user-add-form-email").val(sp.db.client.list[i].email);
						break;
					}
				}
			}
		});
	})
	modal.find(".modal-body input[name='sp-user-add-form-tipo']:checked").trigger("change");

	modal.find(".modal-footer .sp-user-add-form-confirm").on("click",function(){
		var nome 			= modal.find(".modal-body #sp-user-add-form-nome");
		var email 			= modal.find(".modal-body #sp-user-add-form-email");
		var tipo 			= modal.find(".modal-body input[name='sp-user-add-form-tipo']:checked").val();
		var notifications 	= modal.find(".modal-body input#sp-user-add-form-notifications").is(":checked");
		var client_id		= modal.find(".modal-body #sp-user-add-form-client .selection").attr("data-id");
		var grupo 			= modal.find(".modal-body #sp-user-add-form-grupo").val() || '';
		
		modal.find(".modal-body .sp-user-add-form .has-error").removeClass("has-error");
		modal.find(".modal-body .sp-user-add-form .error-message").remove();
		
		var error_counter = 0;
		if(nome.val()==="") {
			nome.closest(".form-group").addClass("has-error");
			nome.after('<small class="error-message text-danger">Campo obrigatório</small>')
			error_counter++;
		}
		if(email.val()==="") {
			email.closest(".form-group").addClass("has-error");
			email.after('<small class="error-message text-danger">Campo obrigatório</small>')
			error_counter++;
		}
		else if(!sp.utils.validate.email(email.val())) {
			email.closest(".form-group").addClass("has-error");
			email.after('<small class="error-message text-danger">E-mail inválido</small>')
			error_counter++;
		}
		if(tipo == sp.db.users.types.client && client_id=="") {
			modal.find(".modal-body #sp-user-add-form-client").addClass("has-error");
			modal.find(".modal-body #sp-user-add-form-client .selection").after('<small class="error-message text-danger"><br>Por favor seleccione um cliente</small>')
			error_counter++;
		}
	
		if(error_counter>0) return false;
		
		nome = nome.val();
		email = email.val();

		bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Confirmar a introdução do novo utilizador?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						sp.loading.show({text: "A adicionar utilizador", overlay: true});
							sp.ws.put({
								url:"users",
								data:{
									"nome":nome,
									"email":email,
									"tipo":tipo,
									"notifications":notifications,
									"grupo":grupo,
									"client_id":client_id
								},
								callbacks:{
									"200":function(data,status,jqXHR){
										toastr.success(data);
										window.location = "#users";
										location.reload();
										sp.loading.hide();
									},
									"400":function(data,status,jqXHR){
										toastr.error("Existem campos obrigatórios por preencher");
										sp.loading.hide();
									},
									"409":function(data,status,jqXHR){
										toastr.error(data.responseText);
										sp.loading.hide();
									},
									"error":function(data,status,jqXHR){
										console.error("Error inserting user");
										sp.loading.hide();
									}
								}
							});
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});
	
	$(".modal:visible").modal("hide");
	
	modal.modal("show");
};
sp.users.edit = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	if(options.id==-1)
	{
		console.error("[sp][sp.users.modal()] invalid user id");
		return false;
	}

	var user = sp.db.users.get_details(options.id);
	
	var modal_id = "sp-users-modal-"+sp.users.last_id++;
	var modal = $('\
	<div class="modal" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Editar utilizador</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer">\
					<button type="button" class="btn btn-default sp-user-edit-form-password" data-email="'+user.email+'"><i class="fa fa-key"></i> Gerar nova password</button>\
					<button type="button" class="btn btn-primary sp-user-edit-form-confirm"><i class="fa fa-check"></i> Confirmar</button>\
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var str = '';
	str += '<table class="table table-striped table-bordered sp-user-edit-form">';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-user-edit-form-tipo" class="control-label"><b>Tipo</b></label></th>';
	str += '		<td colspan="2">';
	str += '			<label style="margin-right:10px;"><input type="radio" name="sp-user-edit-form-tipo" value="0" '+(user.tipo==0?"checked":"")+' /> <span class="label label-warning">Administrador</span></label>';
	str += '			<label style="margin-right:10px;"><input type="radio" name="sp-user-edit-form-tipo" value="1" '+(user.tipo==1?"checked":"")+' /> <span class="label label-primary">Vendedor</span></label>';
	str += '			<label><input type="radio" name="sp-user-edit-form-tipo" value="2" '+(user.tipo==2?"checked":"")+' /> <span class="label label-success">Cliente</span></label>';
	str += '		</td>';
	str += '	</tr>';
	str += '	<tr class="form-group" id="sp-user-edit-form-client">';
	str += '		<th style="vertical-align:middle;"><label class="control-label"><b>Cliente associado</b></label></th>';
	str += '		<td style="vertical-align:middle;" colspan="2"><span class="selection" data-id="'+(user.tipo==2?user.client_id:"")+'">Nenhum cliente seleccionado</span></td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-user-edit-form-nome" class="control-label"><b>Nome</b></label></th>';
	str += '		<td colspan="2"><input type="text" class="form-control" id="sp-user-edit-form-nome" placeholder="Nome" value="'+user.nome+'"></td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-user-edit-form-email" class="control-label"><b>E-mail</b></label></th>';
	str += '		<td colspan="2"><input type="email" class="form-control" id="sp-user-edit-form-email" placeholder="E-mail" value="'+user.email+'"></td>';
	str += '	</tr>';
	//str += '	<tr class="form-group">';
	//str += '		<th style="vertical-align:middle;"><label for="sp-user-edit-form-password" class="control-label"><b>Password</b></label></th>';
	//str += '		<td><input type="password" class="form-control" id="sp-user-edit-form-password" placeholder="Password"></td>';
	//str += '		<td><input type="password" class="form-control" id="sp-user-edit-form-password-confirm" placeholder="Confirmar"></td>';
	//str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-user-edit-form-tipo" class="control-label"><b>Grupo</b></label></th>';
	str += '		<td colspan="2">';
	str += '			<select class="form-control" id="sp-user-edit-form-grupo">';
	str += '				<option value="">Seleccionar</option>';
	for (var i in sp.db.groups.list)
	{
		str += '				<option class="sp-group-user-type sp-group-user-type-'+sp.db.groups.list[i].tipo+'" value="'+sp.db.groups.list[i].id+'" '+(user.grupo==sp.db.groups.list[i].id?"selected":"")+'>'+sp.db.groups.list[i].nome+'</option>';
	}
	str += '			</select>';
	str += '		</td>';
	str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<td class="sp-checkbox" style="vertical-align:middle;" colspan="3">';
	str += '			<input type="checkbox" id="sp-user-edit-form-notifications" '+(user.configs.email_notification.value=="true"?"checked":"")+' /> Enviar notificações de pagamentos por e-mail';
	str += '		</td>';
	str += '	</tr>';
	str += '</table>';

	modal.find(".modal-body").html(str);
	
	if(user.tipo == 2) {
		var client = sp.db.client.get_details(user.client_id);
		modal.find(".modal-body #sp-user-edit-form-client .selection").html(client.nome);
		modal.find(".modal-body #sp-user-edit-form-nome").val(client.nome);
		modal.find(".modal-body #sp-user-edit-form-email").val(client.email);
		modal.find(".modal-body #sp-user-edit-form-nome").prop("disabled",true);
		modal.find(".modal-body #sp-user-edit-form-email").prop("disabled",true);
	}
	
	modal.find(".modal-body input[name='sp-user-edit-form-tipo']").change(function(){
		str = '';
		str += '				<option value="">Seleccionar</option>';
		for (var i in sp.db.groups.list)
		{
			if($(this).val()==sp.db.groups.list[i].tipo)
			str += '				<option class="sp-group-user-type sp-group-user-type-'+sp.db.groups.list[i].tipo+'" value="'+sp.db.groups.list[i].id+'" '+(user.grupo==sp.db.groups.list[i].id?"selected":"")+'>'+sp.db.groups.list[i].nome+'</option>';
		}
		if($(this).val()==sp.db.users.types.client) {
			modal.find(".modal-body #sp-user-edit-form-client .selection").empty();
			modal.find(".modal-body #sp-user-edit-form-nome").val("");
			modal.find(".modal-body #sp-user-edit-form-email").val("");
			if(modal.find(".modal-body #sp-user-edit-form-client .selection").attr("data-id")!="") {
				var client = sp.db.client.get_details(modal.find(".modal-body #sp-user-edit-form-client .selection").attr("data-id"));
				modal.find(".modal-body #sp-user-edit-form-client .selection").html(client.nome);
				modal.find(".modal-body #sp-user-edit-form-nome").val(client.nome);
				modal.find(".modal-body #sp-user-edit-form-email").val(client.email);
			}
			$("#sp-user-edit-form-client").show();
			$("#sp-user-edit-form-nome").prop("disabled",true);
			$("#sp-user-edit-form-email").prop("disabled",true);
		} else {
			$("#sp-user-edit-form-client").hide();
			$("#sp-user-edit-form-nome").prop("disabled",false);
			$("#sp-user-edit-form-email").prop("disabled",false);
		}
		modal.find(".modal-body #sp-user-edit-form-grupo").html(str);
	});

	modal.find(".modal-body #sp-user-edit-form-client button").on("click",function(){
		var selected = [];
		if(modal.find(".modal-body #sp-user-edit-form-client .selection").attr("data-id")!="")
			selected.push(modal.find(".modal-body #sp-user-edit-form-client .selection").attr("data-id"));
		sp.client.select({
			selected: selected,
			multiple: false,
			confirm:function(selected){
				var client = sp.db.client.get_details(selected[0]);
				modal.find(".modal-body #sp-user-edit-form-client .selection").attr("data-id",selected[0]);
				modal.find(".modal-body #sp-user-edit-form-client .selection").html(client.nome);	
				modal.find(".modal-body #sp-user-edit-form-nome").val(client.nome);
				modal.find(".modal-body #sp-user-edit-form-email").val(client.email);
			}
		});
	});
	modal.find(".modal-footer .sp-user-edit-form-password").on("click",function(){
		var e = $(this).attr("data-email");

		bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Gerar e enviar nova password para o e-mail do utilizador?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						sp.loading.show({text: "A gerar nova password", overlay: true});
						$.ajax({
							url: sp.ws_url+"/ws/auth/recover",
							type: 'POST',
							contentType: "application/x-www-form-urlencoded;charset=UTF-8",
							data: {
								"e":e
							},
							success: function(data, status, jqXHR) {
							    toastr.success('As novas credenciais foram enviadas para o e-mail do utilizador');
							    sp.loading.hide();
							},
							error: function(data, status, jqXHR) {
							    toastr.error('Erro a gerar credenciais');
							    sp.loading.hide();
							}
						});
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});
	modal.find(".modal-footer .sp-user-edit-form-confirm").on("click",function(){
		var nome 			= modal.find(".modal-body #sp-user-edit-form-nome");
		var email 			= modal.find(".modal-body #sp-user-edit-form-email");
		var tipo 			= modal.find(".modal-body input[name='sp-user-edit-form-tipo']:checked").val();
		var notifications 	= modal.find(".modal-body input#sp-user-edit-form-notifications").is(":checked");
		var client_id		= modal.find(".modal-body #sp-user-edit-form-client .selection").attr("data-id");
		var grupo 			= modal.find(".modal-body #sp-user-edit-form-grupo").val() || '';
		
		modal.find(".modal-body .sp-user-edit-form .has-error").removeClass("has-error");
		modal.find(".modal-body .sp-user-edit-form .error-message").remove();
		
		var error_counter = 0;
		if(nome.val()==="") {
			nome.closest(".form-group").addClass("has-error");
			nome.after('<small class="error-message text-danger">Campo obrigatório</small>')
			error_counter++;
		}
		if(email.val()==="") {
			email.closest(".form-group").addClass("has-error");
			email.after('<small class="error-message text-danger">Campo obrigatório</small>')
			error_counter++;
		}
		else if(!sp.utils.validate.email(email.val())) {
			email.closest(".form-group").addClass("has-error");
			email.after('<small class="error-message text-danger">E-mail inválido</small>')
			error_counter++;
		}
		if(tipo == sp.db.users.types.client && client_id=="") {
			modal.find(".modal-body #sp-user-edit-form-client").addClass("has-error");
			modal.find(".modal-body #sp-user-edit-form-client .selection").after('<small class="error-message text-danger"><br>Por favor seleccione um cliente</small>')
			error_counter++;
		}
		
		if(error_counter>0) return false;
		
		nome = nome.val();
		email = email.val();

		bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Confirmar as alterações efetuadas?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						sp.loading.show({text: "A aplicar alterações", overlay: true});
							sp.ws.put({
								url:"users",
								data: {
									"id":options.id,
									"nome":nome,
									"email":email,
									"tipo":tipo,
									"notifications":notifications,
									"grupo":grupo,
									"client_id":client_id
								},
								callbacks:{
									"200":function(data,status,jqXHR){
										toastr.success(data);
										window.location = "#users";
										location.reload();
										sp.loading.hide();
									},
									"400":function(data,status,jqXHR){
										toastr.error("Existem campos obrigatórios por preencher");
										sp.loading.hide();
									},
									"409":function(data,status,jqXHR){
										toastr.error(data.responseText);
										sp.loading.hide();
									},
									"error":function(data,status,jqXHR){
										console.error("Error editing user");
										sp.loading.hide();
									}
								}
							});
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});
	
	$(".modal:visible").modal("hide");
	
	modal.modal("show");
	modal.find(".modal-body input[name='sp-user-edit-form-tipo']:checked").trigger("change");
};

sp.debug.register("end");