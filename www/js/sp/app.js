sp.debug.register("start");

$.ajaxSetup ({
    // Disable caching of AJAX responses
    cache: false
});
$(document).ready(function(){
	$(".navbar-toggle").on("click",function(){
		$("html,#wrapper").toggleClass("nav-active");
	});
	$("#sidebar-calendar").datepicker({todayHighlight: true});
	$("#sidebar-calendar").find("th.next").html("<i class='fa fa-angle-right'></i>");
	$("#sidebar-calendar").find("th.prev").html("<i class='fa fa-angle-left'></i>");
	
	$("*[data-toggle=tooltip]").tooltip({html:true});
	
	$("#shopping-cart-container").on("click",function(){window.location.hash="##modal#order#open";});
	$(".shopping-cart-confirm").on("click",function(){sp.order.confirm();});
	$(".shopping-cart-cancel").on("click",function(){sp.order.cancel();});
    $(".shopping-cart-modal-remove-bill_client").on("click",function(){
        sp.order.bill_client = null;
        sp.order.update();
    });
	//document.oncontextmenu = function() {return false;};
//	$(document).mousedown(function(e){ 
//		if( e.button == 2 ) { 
//			alert('Right mouse button!'); 
//			return false; 
//		} 
//		return true; 
//	});
});


sp.loading.show({overlay: true, bgcolor: "#f2f2f2", text: "A obter informações do utilizador ..."});
sp.ws.get({
	url: "auth",
	callbacks: {
		"200": function (data, status, jqXHR)  {
			sp.user = data;

			sp.utils.cookies.set({name: "auth-token",value: localStorage["auth-token"],duration: 604800,path: "/ws/"});

			if(sp.user.apk_version != sp.apk_version && sp.terminal.mobile) {
				message = "";
				message += "A aplicação está desatualizada ("+sp.apk_version+"). Clique para atualizar para a nova versão: "+sp.user.apk_version;

				if(window.plugin) {			
					if(window.plugin.notification) {
						window.plugin.notification.local.onclick = function (id, state, json) {sp.utils.update_apk_version();};
						window.plugin.notification.local.add({ title:'Sparmedix - Atualização',message:message,autoCancel:true });
					}
				}
				//toastr.info(message,'Sparmedix - Atualização',{onclick:function(){sp.utils.update_apk_version();}});
			}
			sp.utils.storage.init();
			$(".sp-user-dynamic-nome").html(sp.user.nome);
			var pending_notes = new Array();
			if(localStorage["notes.pending."+sp.user.id])
				pending_notes = JSON.parse(localStorage["notes.pending."+sp.user.id]);
			if(pending_notes.length>0) {
				$(".sp-dynamic-notes-sync").show();
				$(".sp-dynamic-notes-sync .badge").html(pending_notes.length);
				counter = pending_notes.length;
				var message = 'Existe'+(counter>1?'m':'')+' '+counter+' nota'+(counter>1?'s':'')+' pendente'+(counter>1?'s':'')+', seleccione para sincronizar';
				if(window.plugin) {
					window.plugin.notification.local.onclick = function (id, state, json) {window.location="#sync";};
					window.plugin.notification.local.add({ title:'Sparmedix',message:message,autoCancel:true });
				}
				toastr.info(message,"",{onclick:function(){window.location="#sync";}});
			}
			else {
				$(".sp-dynamic-notes-sync").hide();
			}
			sp.db.images.check();
			sp.ws.get({
				url: "auth/pages",
				callbacks: {
					"200": function (data, status, jqXHR)  {
						sp.user.navigation = data;
						localStorage[sp.utils.md5("u_id")] = sp.user.id;
						localStorage["sp.user"] = JSON.stringify(sp.user);
						str = '';
						for ( var i in sp.user.navigation ){
							if(sp.user.navigation[i].list == 1) str += '<li><a href="'+sp.user.navigation[i].url+'"><i class="fa '+sp.user.navigation[i].icon+'"></i> '+sp.user.navigation[i].nome+'</a></li>';								
						}
						$("#sp-main-nav").prepend(str);
						if($(".side-nav .nav.navbar-nav li").length>0) {
							$(".side-nav .nav.navbar-nav li").on("click",function(){
								if($(".nav-active").length>0) {
									$(".navbar-toggle").trigger("click");	
								}
							});
						}
						sp.loading.hide();
						sp.loading.show({overlay: true, text: "A atualizar base de dados ..."});
						$(window).on("sp-updated-db", function(){
							sp.loading.hide();
							sp.loading.show({overlay: true, text: "A carregar ficheiros ..."});
							load_files();
						   	sp.db.timeout_tick();
						});
						if(!sp.db.update()) {
							toastr.error('Erro ao inicializar a base de dados');
						}
					}
				}
			});
		},
		"error": function (data, status, jqXHR)  {
			sp.user = {};
			sp.user.id = -1;
			if(localStorage[sp.utils.md5("u_id")]) {
				sp.user.id = localStorage[sp.utils.md5("u_id")];
				sp.utils.storage.init();
			}
			if(localStorage["auth-token"] && localStorage["sp.user"]) {
				sp.user = JSON.parse(localStorage["sp.user"]);
				str = '';
				for ( var i in sp.user.navigation )
					if(sp.user.navigation[i].list == 1) str += '<li><a href="'+sp.user.navigation[i].url+'"><i class="fa '+sp.user.navigation[i].icon+'"></i> '+sp.user.navigation[i].nome+'</a></li>';
				$("#sp-main-nav").prepend(str);
				$(".side-nav .nav.navbar-nav li").on("click",function(){
					if($(".nav-active").length>0)
						$(".navbar-toggle").trigger("click");
				});
				sp.loading.hide();
				sp.loading.show({overlay: true, text: "A atualizar base de dados ..."});
				$(window).on("sp-updated-db", function(){
					sp.loading.hide();
					sp.loading.show({overlay: true, text: "A carregar ficheiros ..."});
					load_files();
					sp.db.timeout_tick();
				});
				if(!sp.db.update()) {
					toastr.error('Erro ao inicializar a base de dados');
				}
			}
			else {
				var message = "";
				if(data.responseText == "") message = "Erro na comunicacao com o servidor de autenticacao";
				else message = data.responseText;
				sp.utils.login_message({message:message});
				localStorage.removeItem("auth-token");
				window.location = "login.html";
				return;
			}
		}
	}
});
function load_files() {
	sp.loading.hide();
	//sp.db.images.update_list();
	if(window.location.hash=="")
		window.location="#dashboard";
	sp.navigation.load(true);
	sp.db.timeout_tick();
}

sp.debug.register("end");