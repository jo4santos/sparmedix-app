sp.debug.register("start");

sp.notes = {last_id:0};
sp.notes.modal = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	if(options.id==-1)
	{
		console.error("[sp][sp.notes.modal()] invalid note id");
		return false;
	}
	sp.loading.show({text:"A obter informações da nota"});
	var modal_id = "sp-client-modal-"+sp.notes.last_id++;

	var note = sp.db.notes.get_details(options.id);
	var client = sp.db.client.get_details(note.client_id);
	var user = sp.db.users.get_details(note.user_id);
	var products = sp.db.notes.get_products(options.id);
	
	var modal = $('\
	<div class="modal " id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Detalhes da encomenda</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer">\
						<button type="button" class="btn btn-primary sp-notes-modal-add-to-order"><i class="fa fa-shopping-cart"></i> Adicionar</button>\
						<button type="button" class="btn btn-danger sp-notes-modal-reject" data-estado="'+note.estado+'"><i class="fa fa-times"></i> Cancelar</button>\
						<button type="button" class="btn btn-success sp-notes-modal-close"><i class="fa fa-check"></i> Fechar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	
	var str = '';
	str += '<table class="table table-striped table-bordered">';
	str += '<tr><th><b>Número</b></th><td>'+note.codigo;
	if(!sp.terminal.mobile) {
		str += ' <a style="float:right;" href="'+sp.ws_url+"/ws/notes/pdf/"+note.id+'" target="_blank"><i class="fa fa-file-text-o"></i> PDF</a>';
	}
	else {
		str += ' <a class="sp-notes-modal-pdf" style="float:right;" href="'+sp.ws_url+"/ws/notes/pdf/"+note.id+'" data-filename="'+note.codigo+'" target="_blank"><i class="fa fa-file-text-o"></i> PDF</a>';
	}
	str += '</td></tr>';
	str += '<tr><th><b>Estado</b></th><td>';
	if(note.estado == 0)
		str += ' <span style="font-size:.9em" class="label label-default"><i class="fa fa-clock-o"></i> Pendente</span>';
	else if(note.estado == 1)
		str += ' <span style="font-size:.9em" class="label label-warning"><i class="fa fa-credit-card"></i> Em pagamento</span>';
	else if(note.estado == 2)
		str += ' <span style="font-size:.9em" class="label label-primary"><i class="fa fa-truck"></i> Pago, em progresso</span>';
	else if(note.estado == 3) {
		str += ' <span style="font-size:.9em" class="label label-danger"><i class="fa fa-times"></i> Rejeitada</span>';
		if(note.reject_user_id) {
			str += '<br><small>Em '+note.reject_data+" por "+note.reject_user_name;
			if(note.reject_user_id==note.user_id) {
				str += ' (Vendedor)</small>';
			}
			else if(note.reject_user_id==note.client_id) {
				str += ' (Cliente)</small>';
			}
			else {
				str += ' (Administração)</small>';
			}
		}
	}
	else if(note.estado >= 4) {
		str += ' <span style="font-size:.9em" class="label label-success"><i class="fa fa-check"></i> Tratada</span>';
		if(note.close_user_id) {
			str += '<br><small>Em '+note.close_data+" por "+note.close_user_name;
			if(note.close_user_id==note.user_id) {
				str += ' (Vendedor)</small>';
			}
			else if(note.close_user_id==note.client_id) {
				str += ' (Cliente)</small>';
			}
			else {
				str += ' (Administração)</small>';
			}
		}
	}
	str += '</td></tr>';
	str += '<tr><th><b>Data criação</b></th><td class="sp-data">'+note.data+'</td></tr>';
	if(note.data_payment)
		str += '<tr><th><b>Data pagamento</b></th><td>'+note.data_payment+'</td></tr>';
	
	var client = sp.db.client.get_details(note.client_id);
	if(client && client.nome == note.client_nome) {
		str += '<tr><th><b>Cliente</b></th><td class="sp-cliente"><a href="##modal#client#'+client.id+'#open">'+note.client_nome+'</a></td></tr>';	
	}
	else {
		str += '<tr><th><b>Cliente</b></th><td class="sp-cliente">'+note.client_nome+'</td></tr>';
	}
	if(note.payee_id != note.client_id) {
		str += '<tr><th><b>Faturado a</b></th><td class="sp-cliente">'+note.payee_nome+'</td></tr>';
	}
	
	if (sp.user.tipo!="2") {
		var user = sp.db.users.get_details(note.user_id);
		if(user && user.nome == note.user_nome) {
			str += '<tr data-sp-allow="01"><th><b>Vendedor</b></th><td class="sp-vendedor"><a href="##modal#user#'+user.id+'#open">'+user.nome+'</a></td></tr>';	
		}
		else {
			str += '<tr data-sp-allow="01"><th><b>Vendedor</b></th><td class="sp-vendedor">'+note.user_nome+'</td></tr>';
		}
	}
	str += '</table>';

	if(note.estado == 1 && note.payee_id == note.client_id) {
		str += '<div class="modal-btn"><a class="btn btn-default" href="##modal#mb#'+note.id+'#open"><i class="fa fa-credit-card"></i> Obter dados para pagamento</a></div>';
	}
		
	str += '<h5 class="sp-header"><i class="fa fa-list-alt"></i> Produtos <div class="header-right"><span>Total</span> <span class="label label-success">'+sp.utils.format_price(note.preco)+' &euro;</span></div></h5>';
	str += '<table class="table table-striped table-bordered">';
	
	for(var i in products) 
	{		
		product = sp.db.products.get_details(products[i].product_id);
		var value_price = parseFloat(products[i].preco)*parseFloat(products[i].quantidade)*(1-parseFloat(products[i].discount)/100);
		var value_iva_unit = (parseFloat(products[i].iva)/100)*parseFloat(products[i].preco);
		var value_iva = value_iva_unit*parseFloat(products[i].quantidade)*(1-parseFloat(products[i].discount)/100);
		value_iva = parseFloat((value_iva)*100)/100;
		value_iva_unit = parseFloat((value_iva_unit)*100)/100;
		final_price = (value_price)*100/100+value_iva;
		var price_tooltip = "title='Valor: "+products[i].quantidade+" x "+products[i].preco+" &euro; = "+value_price+" &euro;<br>+<br>IVA: "+products[i].quantidade+" x "+value_iva_unit+" &euro; = "+value_iva+" &euro;'";
		str += '<tr>';
		if(product.codigo) {
			str += '<td style="width:10px;"><img class="sp-product-image-popup" data-image="'+product.codigo+".jpg"+'" style="width:3em;height:3em;" src="'+sp.db.images.get(product.codigo+".jpg")+'" /></td>';
		}
		str += '<td style="vertical-align:middle;">'+products[i].quantidade+'x <!--<a href="##modal#product#'+products[i].product_id+'#open">-->'+products[i].product_nome+'<!--</a>-->'+(products[i].bonus>0?' <span class="label label-primary">+'+products[i].bonus+' bonus</span>':'')+(products[i].discount>0?' <span class="label label-success">-'+products[i].discount+'%</span>':'')+'</td>';
		str += '<td style="vertical-align:middle;" nowrap class="visible-lg visible-md visible-sm">'+sp.utils.format_price(products[i].preco)+' &euro;</td>';
		str += '<td style="vertical-align:middle;" nowrap class="visible-lg visible-md visible-sm">'+products[i].iva+'%</td>'; 
		str += '<td style="vertical-align:middle;text-align:center;width:100px;"><span style="font-size:1em;display:block;" class="label label-success sp-tooltip" data-toggle="tooltip" '+price_tooltip+'>'+sp.utils.format_price(final_price)+'&euro;</span></td>';
		str += '</tr>';
	}
	
	str += '</table>';
	
	modal.find(".modal-body").html(str);
	modal.find(".modal-body .sp-tooltip").tooltip({html: true});
	
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);

	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	$(".modal:visible").modal("hide");
	
	modal.find(".sp-notes-modal-add-to-order").off("click").on("click",function(){
		bootbox.dialog({
			message: "<div class='alert alert-info'><i class='fa fa-info-circle'></i> Pretende adicionar as quantidades e produtos desta nota à nova encomenda?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						sp.order.add.note(note.id);
						window.location="##";
						sp.order.update();
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});
	if(note.estado!=0 && note.estado!=1) {
		modal.find(".sp-notes-modal-reject").hide();
	}
	if(sp.user.tipo!=sp.db.users.types.admin || note.estado==4) {
		modal.find(".sp-notes-modal-close").hide();
	}
	if(sp.terminal.mobile && !sp.terminal.connection) {
		modal.find(".sp-notes-modal-reject[data-estado!='0']").hide();
		modal.find(".sp-notes-modal-close").hide();
	}

	modal.find(".sp-notes-modal-reject").off("click").on("click",function(){
		bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Confirma que pretende cancelar esta encomenda?</div>",
			title: "Confirmar",
			animate:false,
			onEscape: function() {},
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						sp.db.notes.reject(note.id);
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});

	modal.find(".sp-notes-modal-close").off("click").on("click",function(){
		bootbox.dialog({
			message: "<div class='alert alert-info'><i class='fa fa-info-circle'></i> Confirma que a encomenda foi tratada e pode ser fechada?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {
						sp.db.notes.close(note.id);
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});

	if(sp.terminal.mobile) {
		modal.find(".sp-notes-modal-pdf").off("click").on("click",function(e){
			e.preventDefault();
			var filename = $(this).attr("data-filename");
			var href = $(this).attr("href");
			bootbox.dialog({
				message: "<div class='alert alert-info'><i class='fa fa-info-circle'></i> Confirma que a encomenda foi tratada e pode ser fechada?</div>",
				title: "Confirmar",
				buttons: {
					success: {
						label: "<i class='fa fa-check'></i> Confirmar",
						className: "btn-primary",
						callback: function() {
							var filename = "file:///storage/emulated/0/com.sparmedix.orders/pdf/"+filename;
							var fileTransfer = new FileTransfer();
					 		sp.loading.show({overlay: true, text: "A descarregar ficheiro PDF ..."});
							fileTransfer.download(
							    href,
							    filename,
							    function(entry) {
							    	sp.loading.hide();
							    	toastr.success("PDF descarregado para "+entry.fullPath);
							    	window.plugins.webintent.startActivity({
							            action: window.plugins.webintent.ACTION_VIEW,
							            url: filename,
							            type: 'application/pdf'
							            },
							            function(){},
							            function(e){}
							        );
							    },
							    function(error) {
							        toastr.error("Erro no download:<br>Source: " + error.source+"<br>Target: " + error.target + "<br>Code" + error.code);
							    },
							    {
							        headers: {
							            "Connection": "close"
							        }
							    }		    
							);

						}
					},
					danger: {
						label: "<i class='fa fa-times'></i> Cancelar",
						className: "btn-default"
					}
				}
			});
		});
	}
	
	modal.modal("show");
	sp.loading.hide();	
};

sp.notes.modal.mb = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	if(options.id==-1)
	{
		console.error("[sp][sp.notes.modal.mb()] invalid note id");
		return false;
	}
	var note = sp.db.notes.get_details(options.id);
	if(note.estado == 0) {
		toastr.error("Finalizar encomenda para obter dados de pagamento");
		return false;
	}
	else if(note.estado != 1) {
		toastr.error("Pagamento já efetuado e processado");
		return false;
	}
	sp.loading.show({text:"A obter dados de pagamento"});
	var str = '';
	str += '<table class="table table-striped table-bordered">';
	str += '<tr><th><b>Encomenda:</b></th><td class="sp-data">'+note.codigo+'</td></tr>';
	str += '<tr><th><b>Data criação:</b></th><td class="sp-data">'+note.data+'</td></tr>';
	str += '</table>';
	str += '<table class="table table-bordered" style="margin-bottom:0;">';
	str += '	<thead><tr><th style="background:#293541;border-color:#293541;color:rgba(255,255,255,.7);font-size:.85em;text-align:center;" colspan="3">Pagamento por Multibanco ou Homebanking</th></tr></thead>';
	str += '	<tr>';
	str += '		<td rowspan="3" style="width:120px;padding:10px;">';
	str += '			<img src="images/mb-icon.svg" style="width:100px;height:100px;" />';
	str += '		</td>';
	str += '		<th style="text-transform:uppercase;font-size:.9em;vertical-align:middle;font-weight:700;">Entidade:</th>';
	str += '		<td style="vertical-align:middle;text-align:center;">'+sp.order.entidade+'</td>';
	str += '	</tr>';
	str += '	<tr>';
	str += '		<th style="text-transform:uppercase;font-size:.9em;vertical-align:middle;font-weight:700;">Referência:</th>';
	str += '		<td style="vertical-align:middle;text-align:center;">'+note.referencia+'</td>';
	str += '	</tr>';
	str += '	<tr>';
	str += '		<th style="text-transform:uppercase;font-size:.9em;vertical-align:middle;font-weight:700;">Valor:</th>';
	str += '		<td style="vertical-align:middle;text-align:center;">'+sp.utils.format_price(note.preco)+' &euro;</td>';
	str += '	</tr>';
	str += '	<tfoot><tr><th style="background:#293541;border-color:#293541;color:rgba(255,255,255,.7);font-size:.85em;text-align:center;" colspan="3">O talão emitido pela caixa automática faz prova de pagamento.<br>Conserve-o.</th></tr></tfoot>';
	str += '</table>';
	bootbox.dialog({
		message: str,
		title: 'Dados de pagamento'
	});
	sp.loading.hide();
};

sp.debug.register("end");