sp.debug.register("start");

sp.search = {
	timeout: null,
	last: "",
	curr: ""
};
$("#sp-search-input").on("keyup change",function() {
	clearTimeout(sp.search.timeout);
	sp.search.timeout = setTimeout(sp.search.update,500);
});

sp.search.update = function() {
	sp.search.curr = $("#sp-search-input").val();
	if(sp.search.curr != sp.search.last) {
		sp.search.last = sp.search.curr;
		if(sp.search.curr=="") {
			$(".sp-search-content").hide();
			$(".sp-search-content .sp-search-clients div").empty();
			$(".sp-search-content .sp-search-products div").empty();
			$(".sp-search-content .sp-search-notes div").empty();
			$(".sp-search-info").show();
			return false;
		}
		else {
			$(".sp-search-content").show();
			$(".sp-search-info").hide();
		}
		$(".sp-search-content .alert").remove();
		clients = sp.db.client.filter({
			all: 		sp.search.curr,
			nome: 		true,
			codigo: 	true,
			favorites: 	false
		});
		products = sp.db.products.filter({
			all: 		sp.search.curr,
			nome: 		true,
			codigo: 	true,
			favorites: 	false
		});
		notes = sp.db.notes.filter({
			all: 		sp.search.curr,
			numero:		true,
			codigo: 	false
		});
		$(".sp-search-content .sp-search-clients div").empty();
		$(".sp-search-content .sp-search-clients h6").html("Clientes ("+clients.length+")");
		var counter = 0;
		for ( var i in clients ) {
			var client = clients[i];
			$(".sp-search-content .sp-search-clients div").append("<a class='sp-search-element' href='##modal#client#"+client.id+"#open'>"+client.nome+"</a>");
			counter ++;
		}
		if(clients.length==0) {
			//$(".sp-search-content .sp-search-clients div").append("<div class='alert alert-warning'>Nenhum encontrado</div>");	
			$(".sp-search-content .sp-search-clients").hide();
		}
		else {
			$(".sp-search-content .sp-search-clients div").append("<button class='btn btn-success btn-block btn-sm'>Mostrar mais 5...</button>");
			$(".sp-search-content .sp-search-clients").show();
		}
		$(".sp-search-content .sp-search-clients button").on("click",function(){
			$(".sp-search-content .sp-search-clients .sp-search-element:not(.visible):lt(5)").addClass("visible");
			if($(".sp-search-content .sp-search-clients .sp-search-element:not(.visible)").length==0)
				$(this).hide();
		}).trigger("click");
		$(".sp-search-content .sp-search-products div").empty();
		$(".sp-search-content .sp-search-products h6").html("Produtos ("+products.length+")");
		var counter = 0;
		for ( var i in products ) {
			var product = products[i];
			$(".sp-search-content .sp-search-products div").append("<a class='sp-search-element' href='##modal#product#"+product.id+"#open'>"+product.nome+"</a>");
			counter ++;
		}
		if(products.length==0) {
			//$(".sp-search-content .sp-search-products div").append("<div class='alert alert-warning'>Nenhum encontrado</div>");	
			$(".sp-search-content .sp-search-products").hide();
		}
		else {
			$(".sp-search-content .sp-search-products div").append("<button class='btn btn-success btn-block btn-sm'>Mostrar mais 5...</button>");
			$(".sp-search-content .sp-search-products").show();
		}
		$(".sp-search-content .sp-search-products button").on("click",function(){
			$(".sp-search-content .sp-search-products .sp-search-element:not(.visible):lt(5)").addClass("visible");
			if($(".sp-search-content .sp-search-products .sp-search-element:not(.visible)").length==0)
				$(this).hide();
		}).trigger("click");
		$(".sp-search-content .sp-search-notes div").empty();
		$(".sp-search-content .sp-search-notes h6").html("Encomendas ("+notes.length+")");
		var counter = 0;
		for ( var i in notes ) {
			var note = notes[i];
			$(".sp-search-content .sp-search-notes div").append("<a class='sp-search-element' href='##modal#note#"+note.id+"#open'>"+note.codigo+"</a>");
			counter ++;
		}
		if(notes.length==0) {
			//$(".sp-search-content .sp-search-notes div").append("<div class='alert alert-warning'>Nenhuma encontrada</div>");	
			$(".sp-search-content .sp-search-notes").hide();
		}
		else {
			$(".sp-search-content .sp-search-notes div").append("<button class='btn btn-success btn-block btn-sm'>Mostrar mais 5...</button>");
			$(".sp-search-content .sp-search-notes").show();
		}
		$(".sp-search-content .sp-search-notes button").on("click",function(){
			$(".sp-search-content .sp-search-notes .sp-search-element:not(.visible):lt(5)").addClass("visible");
			if($(".sp-search-content .sp-search-notes .sp-search-element:not(.visible)").length==0)
				$(this).hide();
		}).trigger("click");

		if(clients.length+products.length+notes.length==0) {
			$(".sp-search-content").append("<div class='alert alert-warning'>Nenhuma correspondência</div>");	
		} else {
			$(".sp-search-content .alert").remove();
		}
	}
}
var str = "";
str += "<button id='sp-search-close' class='btn btn-primary btn-xs'><i class='fa fa-times'></i></button>";
str += "<div class='sp-search-content' style='display:none;'>";
str += "	<div class='sp-search-clients'><h6>Clientes</h6><div></div></div>";
str += "	<div class='sp-search-products'><h6>Produtos</h6><div></div></div>";
str += "	<div class='sp-search-notes'><h6>Encomendas</h6><div></div></div>";
str += "	</div>";
str += "</div>";
str += "<div class='sp-search-info'><div class='alert alert-info'>Inserir termo para pesquisa</div></div>";
$("#sp-search-input").popover({
	placement:"bottom",
	trigger:"manual",
	html:true,
	title:"Pesquisa detalhada",
	content:str
});
$("#sp-search-input").on("focus",function(){
	$("#sp-search-input").popover("show");
	$("#sp-search-close").on("click",function(){
		$("#sp-search-input").popover("hide").val("");
	});
});

sp.debug.register("end");