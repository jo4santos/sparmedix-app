sp.debug.register("start");
sp.client = {last_id:0};
sp.client.modal = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	if(options.id==-1)
	{
		console.error("[sp][sp.client.modal()] invalid client id");
		return false;
	}
	sp.loading.show({text:"A obter informações do cliente"});
	var modal_id = "sp-client-modal-"+sp.client.last_id++;
	var modal = $('\
	<div class="modal" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Detalhes do cliente</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div data-sp-allow="01" class="modal-footer">\
					<button type="button" class="btn btn-primary sp-client-details-add"><i class="fa fa-shopping-cart"></i> Cliente final</button>\
					<button type="button" class="btn btn-primary sp-client-details-add_bill"><i class="fa fa-shopping-cart"></i> Cliente faturação</button>\
				</div>\
			</div>\
		</div>\
	</div>');

    if(sp.order.activeSelect == "final") {
        modal.find(".modal-footer").html('<span style="font-size:12px">Será seleccionado como cliente final</span><button type="button" class="btn btn-primary sp-client-details-add"><i class="fa fa-shopping-cart"></i> Confirmar</button>');
    }
    else if(sp.order.activeSelect == "bill") {
        modal.find(".modal-footer").html('<span style="font-size:12px">Será seleccionado como cliente para faturação</span><button type="button" class="btn btn-primary sp-client-details-add_bill"><i class="fa fa-shopping-cart"></i> Confirmar</button>');
    }

	if (sp.user.tipo=="2") modal.find(".modal-footer").remove();
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var client = sp.db.client.get_details(options.id);
	var notes = sp.db.notes.get_by_client_id(options.id);
	
	var str = '';
	str += '<table class="table table-striped table-bordered">';
	str += '<tr><th><b>Código</b></th><td class="sp-codigo">'+client.codigo+'</td></tr>';
	str += '<tr><th><b>Nome</b></th><td class="sp-nome">'+client.nome+'</td></tr>';
	str += '<tr><th><b>NIF</b></th><td class="sp-nif">'+client.nif+'</td></tr>';
	if(client.email!="")
		str += '<tr><th><b>E-mail</b></th><td class="sp-email">'+client.email+'</td></tr>';
	if(client.responsavel!="")
		str += '<tr><th><b>Responsável</b></th><td class="sp-responsavel">'+client.responsavel+'</td></tr>';
	str += '<tr><th><b>Morada</b></th><td class="sp-morada">'+client.morada;
	if(client.cdpostal!="" || client.local!="") {
		str += '<br>';
		if(client.cdpostal!="") str += client.cdpostal;
		if(client.local!="") str += ' '+client.local;
	}
	str += '</td></tr>';
    if(sp.order.client == client.id) {
        str += '<tr><td colspan="2" class="text-right">Selecionado como cliente final</td></tr>';
    }
    else if(sp.order.bill_client == client.id) {
        str += '<tr><td colspan="2" class="text-right">Selecionado como cliente para faturação</td></tr>';
    }
	str += '</table>';
	str += '<h5 class="sp-header"><i class="fa fa-shopping-cart"></i> Encomendas <small style="display:inline-block;margin-left:10px">('+notes.length+')</small></h5>';
	if(notes.length>0)
	{
		str += '<table class="table table-striped table-bordered sp-notes" style="margin-bottom:10px">';
		for(var i in notes)
		{
			var note = notes[i];
			str += '<tr class="sp-animate sp-slide">';
			str += '<td class="text-center" style="width:30px;vertical-align:middle;" nowrap>';
			if(note.estado == 0)
				str += '<span class="label label-default" data-toggle="tooltip" title="Pendente"><i class="fa fa-clock-o"></i></span>';
			else if(note.estado == 1)
				str += '<span class="label label-warning" data-toggle="tooltip" title="Em pagamento"><i class="fa fa-credit-card"></i></span>';
			else if(note.estado == 2)
				str += '<span class="label label-primary" data-toggle="tooltip" title="Pago, em progresso"><i class="fa fa-truck"></i></span>';
			else if(note.estado == 3)
				str += '<span class="label label-danger" data-toggle="tooltip" title="Rejeitada"><i class="fa fa-times"></i></span>';
			else if(note.estado >= 4)
				str += '<span class="label label-success" data-toggle="tooltip" title="Tratada"><i class="fa fa-check"></i></span>';
			str += '</td>';
			str += '<td><a href="##modal#note#'+note.id+'#open">'+note.codigo+'</a><br><small style="display:block;line-height:1em;" class="text-muted">'+note.data+'</small></td>';
			str += '<td style="vertical-align:middle;text-align:center;width:100px;">';
			str += '<span class="label label-success" style="font-size:1em;display:block;">'+sp.utils.format_price(note.preco)+' &euro;</span>';
			str += '</td>';
			str += '</tr>';
		}
		str += '</table>';
		str += '<div class="modal-btn"><button class="btn btn-default sp-load-more"><i class="fa fa-plus"></i> Ver mais encomendas</button></div>';
	}
	else
	{
		str += '<div class="alert alert-info"><i class="fa fa-info-circle"></i> Sem encomendas registadas</div>';
	}
	modal.find(".modal-body").html(str);

	modal.find(".modal-body").find(".sp-load-more").on("click",function(){
		modal.find(".modal-body").find(".sp-notes tr:not(.visible):lt(5)").addClass("visible");
		if(modal.find(".modal-body").find(".sp-notes tr:not(.visible)").length==0) $(this).hide();
	}).trigger("click");

    modal.find(".modal-footer .sp-client-details-add").on("click",function(){
        sp.order.client = client.id;
        toastr.success("<span class='toastr-label'>Cliente:</span> "+client.nome,"Adicionado à encomenda");
        window.location="#clients#modal#order#open";
        if(sp.order.activeSelect != "") {
            sp.order.activeSelect = "";

        }
        sp.order.update();
    });

    modal.find(".modal-footer .sp-client-details-add_bill").on("click",function(){
        sp.order.bill_client = client.id;
        toastr.success("<span class='toastr-label'>Cliente:</span> "+client.nome,"Adicionado à encomenda");
        window.location="#clients#modal#order#open";
        sp.order.update();
    });
	
	$(".modal:visible").modal("hide");
	
	modal.modal("show");
	sp.loading.hide();	
};

sp.client.list = function(clients) {
	options = {};
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	sp.loading.show({text:"A obter informações do utilizador"});
	var modal_id = "sp-users-modal-"+sp.users.last_id++;
	var modal = $('\
	<div class="modal " id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Clientes</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var str = '';
	str += '<table class="table table-striped table-bordered" style="margin:0">';
	str += '<thead>';
	str += '<tr><th><small><b>Nome</b></small></th><th><small><b>Código</b></small></th><th><small><b>NIF</b></small></th></tr>';
	str += '</thead>';
	str += '<tbody>';
	for ( var i in clients ) {
		var client = sp.db.client.get_details(clients[i]);
		str += "<tr>";
		str += '<th><a href="##modal#client#'+client.id+'#open">'+client.nome+'</a></th>';
		str += "<td>"+client.codigo+"</td>";
		str += "<td>"+client.nif+"</td>";
		str += "</tr>";
	}
	str += '</tbody>';
	str += '</table>';
	
	modal.find(".modal-body").html(str);
	
	$(".modal:visible").modal("hide");
	
	modal.modal("show");
	sp.loading.hide();
};

sp.client.select = function(options) {

	if(sp.db.client.list.length == 0) {
		toastr.error("Não existem clientes para escolher");
		return false;
	}

	var options = options || {};
	options.selected = options.selected || new Array();
	options.multiple = options.multiple || false;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	options.confirm = options.confirm || function(){};
	
	$(".sp-client-select").remove();
	var modal_id = "sp-client-select-"+sp.client.last_id++;
	var modal = $('\
	<div class="modal sp-client-select" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<h4 class="modal-title" id="'+modal_id+'-label">Seleccionar Clientes</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer sp-client-select-btns">\
					<button type="button" class="btn btn-primary sp-confirm"><i class="fa fa-check"></i> Confirmar</button>\
					<button type="button" class="btn btn-default sp-cancel"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false,keyboard:false,backdrop:'static'});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var str = '';
	str += '	<div style="padding: 5px 10px;background: #eee;">';
	str += '		<div class="btn-group btn-group-justified sp-client-select-btns">';
	if(options.multiple === true) {
		str += '		<a class="btn btn-default sp-select-all"><i class="fa fa-check-square-o"></i> Todos</a>';
		str += '		<a class="btn btn-default sp-select-none"><i class="fa fa-square-o"></i> Nenhum</a>';
	}
	str += '		</div>';
	str += '	</div>';
	str += '	<div class="sp-client-select-search">';
	str += '		<div class="input-group">';
	str += '			<span class="input-group-addon"><i class="fa fa-search"></i></span>';
	str += '			<input type="search" class="form-control" placeholder="Pesquisar cliente">';
	str += '			<span class="input-group-addon btn btn-default"><i class="fa fa-times"></i></span>';
	str += '		</div>';
	str += '	</div>';
	str += '	<div style="padding: 10px;background: #eee;">';
	str += '		<div class="sp-client-select-name-filter">';
	str += '			<table style="margin:0;">';
	str += '				<tr>';
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	str += '					<td nowrap>*</td>';
	str += '					<td nowrap>0-9</td>';
	for (var i=0;i<possible.length;i++)
	{
		str += '				<td class="'+(i==0?"active":"")+'">'+possible[i]+'</td>';
	}
	str += '				</tr>';
	str += '			</table>';
	str += '		</div>';
	str += '	</div>';
	str += '	<div class="sp-client-select-list">';
	str += '		<table class="table table-striped table-condensed" style="margin:0;">';
	str += '			<tbody>';
	/*
	var last_initial = sp.db.client.list[0].nome[0].toUpperCase();
	str += '				<tr style="'+(last_initial!="A"?"display:none":"")+'" class="sp-separator sp-initial-'+last_initial+'"><th colspan="20">'+last_initial+'</th></tr>';
	for ( var i in sp.db.client.list ) {
		var client = sp.db.client.list[i];
		if(last_initial!=client.nome[0]) {
			last_initial = client.nome[0].toUpperCase();
			str += '		<tr style="'+(last_initial!="A"?"display:none":"")+'" class="sp-separator sp-initial-'+last_initial+'"><th colspan="20">'+last_initial+'</th></tr>';
		}
		str += '			<tr style="'+(last_initial!="A"?"display:none":"")+'" data-id="'+client.id+'" class="sp-initial-'+last_initial+' '+((options.selected).indexOf(""+client.id)!=-1?"selected":"")+'">';
		str += '				<td class="sp-select-cell">';
		str += '					<span class="label label-default">'+client.codigo+'</span>';
		str += '				</td>';
		str += '				<td class="sp-select-cell">';
		str += '					<span>'+client.nome+'<br><small class="text-muted">NIF:'+client.nif+'</small></span>';
		str += '				</td>';
		str += '				<td class="sp-select-cell"><span><i class="fa"></i></span></td>';
		str += "			</tr>";
	}
	*/
	str += '			</tbody>';
	str += '		</table>';
	str += '		<div class="alert alert-info" style="margin:10px;display:none"><i class="fa fa-info-circle"></i> Não foram encontrados clientes</div>';
	str += '	</div>';
	
	modal.find(".modal-body").html(str);
	
	var sp_clients_select_search_input_timeout;
	modal.find(".modal-body .sp-client-select-search input").on("keyup",function(){
		if($(this).val()=="") {
			modal.find(".modal-body .sp-client-select-search .btn").attr("disabled","disabled");
			modal.find(".modal-body .sp-client-select-name-filter td.active").removeClass("active");
			modal.find(".modal-body .sp-client-select-name-filter td:eq(2)").addClass("active");
		}
		else {
			modal.find(".modal-body .sp-client-select-search .btn").removeAttr("disabled");
			modal.find(".modal-body .sp-client-select-name-filter td.active").removeClass("active");
			modal.find(".modal-body .sp-client-select-name-filter td:first").addClass("active");
		}
		
		clearTimeout(sp_clients_select_search_input_timeout);
		sp_clients_select_search_input_timeout = setTimeout(function(){
			modal.find(".modal-body .sp-client-select-list").trigger("update-content");
		},500);
	});
	modal.find(".modal-body .sp-client-select-search .btn").on("click",function(){
		modal.find(".modal-body .sp-client-select-search input").val("").trigger("keyup");
	});
	
	modal.find(".modal-body .sp-client-select-list").on("update-content",function(){
		var num_visible = 10;
		var letter = modal.find(".modal-body .sp-client-select-name-filter td.active").html().toUpperCase();
		if(letter=="*")letter="";
		var clients = sp.db.client.filter({
			initial: letter,
			all: ""+modal.find(".modal-body .sp-client-select-search input").val(),
			nome: true,
			nif: true,
			codigo: true
		});
		modal.find(".modal-body .sp-client-select-list tbody").empty();
		if(clients.length == 0) {modal.find(".modal-body .sp-client-select-list .alert").show();return false;}
		
		var last_initial = clients[0].nome[0].toUpperCase();
		str = '';
		str += '				<tr class="sp-separator sp-initial-'+last_initial+'"><th colspan="20">'+last_initial+'</th></tr>';
		var counter = 0;
		for ( var i in clients ) {
			var client = clients[i];
			if(last_initial!=client.nome[0].toUpperCase()) {
				last_initial = client.nome[0].toUpperCase();
				str += '		<tr class="sp-separator sp-initial-'+last_initial+'"><th colspan="20">'+last_initial+'</th></tr>';
			}
			str += '			<tr data-id="'+client.id+'" class="'+(counter<num_visible?'visible ':'')+'sp-initial-'+last_initial+' '+((options.selected).indexOf(""+client.id)!=-1?"selected":"")+'">';
			str += '				<td class="sp-select-cell">';
			str += '					<span class="label label-default">'+client.codigo+'</span>';
			str += '				</td>';
			str += '				<td class="sp-select-cell">';
			str += '					<span>'+client.nome+'<br><small class="text-muted">NIF:'+client.nif+'</small>'+(client.local!=""?'<br><small class="text-muted">Localidade:'+client.local+'</small>':'')+'</span>';
			str += '				</td>';
			str += '				<td class="sp-select-cell"><span><i class="fa"></i></span></td>';
			str += "			</tr>";
			counter++;
		}

		if(counter>num_visible)
			str += '<tr class="sp-client-select-more"><td colspan="20"><a class="btn btn-success btn-block">Apresentar mais ...</a></td></tr>';
		
		modal.find(".modal-body .sp-client-select-list tbody").html(str);
		
		if(clients.length==0) modal.find(".modal-body .sp-client-select-list .alert").show();
		else modal.find(".modal-body .sp-client-select-list .alert").hide();
		
		modal.find(".modal-body .sp-select-cell").on("click",function(){
			if($(this).parent().hasClass("selected")) {
				$(this).parent().removeClass("selected");
				options.selected.splice($.inArray($(this).parent().attr("data-id"), options.selected),1);
			}
			else {			
				if(options.multiple === false) {
					modal.find(".modal-body .sp-client-select-list table tr.selected").removeClass("selected");
					options.selected.splice($.inArray(modal.find(".modal-body .sp-client-select-list table tr.selected").attr("data-id"), options.selected),1);
				}
				$(this).parent().addClass("selected");
				options.selected.push($(this).parent().attr("data-id"));
			}
			
		});

		modal.find(".modal-body .sp-client-select-more a").off("click").on("click",function(){
			if($(".sp-client-select-list tr").length==0)return;
			$(".sp-client-select-list tr:not(.visible):lt("+num_visible+")").addClass("visible");
			if($(".sp-client-select-list tr:not(.visible)").length==0)
				$(this).hide();
		});
	});
	modal.find(".modal-body .sp-select-all").on("click",function(){
		modal.find(".modal-body .sp-client-select-list table tr:not(.selected)").addClass("selected");
		options.selected = sp.db.client.list;
	});
	modal.find(".modal-body .sp-select-none").on("click",function(){
		modal.find(".modal-body .sp-client-select-list table tr.selected").removeClass("selected");
		options.selected = new Array();
	});
	modal.find(".modal-body .sp-client-select-name-filter td").on("click",function(){
		modal.find(".modal-body .sp-client-select-name-filter td.active").removeClass("active");
		$(this).addClass("active");		
		modal.find(".modal-body .sp-client-select-list").trigger("update-content");
	});
	modal.find(".modal-footer .sp-confirm").on("click",function(){
		options.confirm(options.selected);
		modal.modal("hide");
	});
	modal.find(".modal-footer .sp-cancel").on("click",function(){
		modal.modal("hide");
	});
	
	modal.find(".modal-body .sp-client-select-list").trigger("update-content");
	
	modal.modal("show");
};
sp.debug.register("end");