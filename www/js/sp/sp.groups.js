sp.debug.register("start");
sp.groups = {last_id:0};
sp.groups.add = function(options) {
	options = options || {};
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	$(".sp-groups-add").remove();
	var modal_id = "sp-groups-add-"+sp.groups.last_id++;
	var modal = $('\
	<div class="modal sp-groups-add" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Adicionar novo grupo</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer">\
					<button type="button" class="btn btn-primary sp-group-add-form-confirm"><i class="fa fa-check"></i> Confirmar</button>\
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var str = '';
	str += '<h5 class="sp-header"><i class="fa fa-info"></i> Informações gerais</h5>';
	str += '<table class="table table-striped table-bordered sp-group-add-form">';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-group-add-form-nome" class="control-label"><b>Nome</b></label></th>';
	str += '		<td><input type="text" class="form-control" id="sp-group-add-form-nome" placeholder="Nome"></td>';
	str += '	</tr>';
    str += '	<tr class="form-group">';
    str += '		<th style="vertical-align:middle;"><label for="sp-group-add-form-descricao" class="control-label"><b>Descrição</b></label></th>';
    str += '		<td><input type="text" class="form-control" id="sp-group-add-form-descricao" placeholder="Descrição"></td>';
    str += '	</tr>';
    str += '	<tr class="form-group">';
    str += '		<th style="vertical-align:middle;"><label for="sp-group-add-form-lista_distribuicao" class="control-label"><b>Lista distribuicao</b></label></th>';
    str += '		<td><input type="text" class="form-control" id="sp-group-add-form-lista_distribuicao" placeholder="E-mails separados por ;"></td>';
    str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-group-add-form-tipo" class="control-label"><b>Tipo</b></label></th>';
	str += '		<td>';
	str += '			<label style="margin-right:10px;"><input type="radio" name="sp-group-add-form-tipo" value="0" /> <span class="label label-warning">Administradores</span></label>';
	str += '			<label style="margin-right:10px;"><input type="radio" name="sp-group-add-form-tipo" value="1" /> <span class="label label-primary">Vendedores</span></label>';
	str += '			<label><input type="radio" name="sp-group-add-form-tipo" checked value="2" /> <span class="label label-success">Clientes</span></label>';
	str += '		</td>';
	str += '	</tr>';
	str += '</table>';
	str += '<div id="sp-group-add-products-container">';
	str += '	<h5 class="sp-header"><i class="fa fa-list"></i> Produtos visíveis</h5>';
	str += '	<button id="sp-group-add-products" class="btn btn-default"><i class="fa fa-check-square-o"></i> Seleccionar</button>';
	str += '	<span style="margin-left:10px;" id="sp-group-add-products-selected" class="text-muted">Todos os produtos são visíveis</span>';
	str += '	<input type="hidden" id="sp-group-add-products-selected-input" />';
	str += '</div>';
	str += '<div id="sp-group-add-clients-container" style="margin-top:20px;">';
	str += '	<h5 class="sp-header"><i class="fa fa-group"></i> Clientes visíveis</h5>';
	str += '	<button id="sp-group-add-clients" class="btn btn-default"><i class="fa fa-check-square-o"></i> Seleccionar</button>';
	str += '	<span style="margin-left:10px;" id="sp-group-add-clients-selected" class="text-muted">Todos os clientes são visíveis</span>';
	str += '	<input type="hidden" id="sp-group-add-clients-selected-input" />';
	str += '</div>';
	
	modal.find(".modal-body").html(str);
	
	modal.find(".modal-body input[name='sp-group-add-form-tipo']").change(function(){
		str = '';
		str += '				<option value="">Seleccionar</option>';
		for (var i in sp.db.groups.list)
		{
			if($(this).val()==sp.db.groups.list[i].tipo)
			str += '				<option class="sp-group-group-type sp-group-group-type-'+sp.db.groups.list[i].tipo+'" value="'+sp.db.groups.list[i].id+'">'+sp.db.groups.list[i].nome+'</option>';
		}
		modal.find(".modal-body #sp-group-add-form-grupo").html(str);
	});
	modal.find(".modal-body input[name='sp-group-add-form-tipo']:checked").trigger("change");

	modal.find(".modal-body #sp-group-add-products").on("click",function(){
		sp.product.select({
			selected: modal.find(".modal-body #sp-group-add-products-selected-input").val().split(","),
			multiple: true,
			confirm:function(selected){
				if(selected.length>0)
				{
					modal.find(".modal-body #sp-group-add-products-selected").html("<a href=''>"+selected.length+" produtos seleccionados</a>");
					if(selected.length==1)
					{
						modal.find(".modal-body #sp-group-add-products-selected").html("<a href=''>"+selected.length+" produto seleccionado</a>");
					}
					modal.find(".modal-body #sp-group-add-products-selected a").on("click",function(e) {
						e.preventDefault();
						modal.find(".modal-body #sp-group-add-products").trigger("click");
					});
				}
				else
				{
					modal.find(".modal-body #sp-group-add-products-selected").html("Todos os produtos são permitidos");
				}
				modal.find(".modal-body #sp-group-add-products-selected-input").val(selected);
			}
		});
	});

	modal.find(".modal-body #sp-group-add-clients").on("click",function(){
		sp.client.select({
			selected: modal.find(".modal-body #sp-group-add-clients-selected-input").val().split(","),
			multiple: true,
			confirm:function(selected){
				if(selected.length>0)
				{
					modal.find(".modal-body #sp-group-add-clients-selected").html("<a href=''>"+selected.length+" clientes seleccionados</a>");
					if(selected.length==1)
					{
						modal.find(".modal-body #sp-group-add-clients-selected").html("<a href=''>"+selected.length+" cliente seleccionado</a>");
					}
					modal.find(".modal-body #sp-group-add-clients-selected a").on("click",function(e) {
						e.preventDefault();
						modal.find(".modal-body #sp-group-add-clients").trigger("click");
					});
				}
				else
				{
					modal.find(".modal-body #sp-group-add-clients-selected").html("Todos os clientes são permitidos");
				}
				modal.find(".modal-body #sp-group-add-clients-selected-input").val(selected);
			}
		});
	});
	
	modal.find(".modal-body input[name='sp-group-add-form-tipo']").change(function(){
		var selected = $(this).val();
		if(selected == sp.db.users.types.client) {
			modal.find(".modal-body #sp-group-add-products-container").show();
			modal.find(".modal-body #sp-group-add-clients-container").hide();
		}
		else if(selected == sp.db.users.types.sales) {
			modal.find(".modal-body #sp-group-add-products-container").show();
			modal.find(".modal-body #sp-group-add-clients-container").show();
		}
		else {
			modal.find(".modal-body #sp-group-add-products-container").hide();
			modal.find(".modal-body #sp-group-add-clients-container").hide();
		}
	});
	modal.find(".modal-body #sp-group-add-products-container").show();
	modal.find(".modal-body #sp-group-add-clients-container").hide();
	
	modal.find(".modal-footer .sp-group-add-form-confirm").on("click",function(){
		var nome 		= modal.find(".modal-body #sp-group-add-form-nome");
        var descricao 	= modal.find(".modal-body #sp-group-add-form-descricao");
        var lista_distribuicao 	= modal.find(".modal-body #sp-group-add-form-lista_distribuicao");
		var tipo 		= modal.find(".modal-body input[name='sp-group-add-form-tipo']:checked").val();
		var clientes 	= modal.find(".modal-body #sp-group-add-clients-selected-input").val().split(",");
		var produtos	= modal.find(".modal-body #sp-group-add-products-selected-input").val().split(",");
		
		modal.find(".modal-body .sp-group-add-form .has-error").removeClass("has-error");
		modal.find(".modal-body .sp-group-add-form .error-message").remove();
		
		var error_counter = 0;
		if(nome.val()==="") {
			nome.closest(".form-group").addClass("has-error");
			nome.after('<small class="error-message text-danger">Campo obrigatório</small>')
			error_counter++;
		}
		if(descricao.val()==="") {
			descricao.closest(".form-group").addClass("has-error");
			descricao.after('<small class="error-message text-danger">Campo obrigatório</small>')
			error_counter++;
		}
		
		if(error_counter>0) return false;
		
		nome = nome.val();
		descricao = descricao.val();
        lista_distribuicao = lista_distribuicao.val();
		
		bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Confirmar a introdução do novo grupo?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {

						sp.ws.put({
							url:"groups",
							data: {
								"nome":nome,
                                "descricao":descricao,
                                "lista_distribuicao":lista_distribuicao,
								"tipo":tipo,
								"clientes":clientes,
								"produtos":produtos
							},
							callbacks:{
								"200":function(data,status,jqXHR){
									toastr.success(data);
									window.location = "#users";
									location.reload();
								},
								"400":function(data,status,jqXHR){
									toastr.error("Existem campos obrigatórios por preencher");
									sp.loading.hide();
								},
								"409":function(data,status,jqXHR){
									toastr.error(data.responseText);
								},
								"error":function(data,status,jqXHR){
									toastr.error(data.responseText);
								}
							}
						});
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});
	
	$(".modal:visible").modal("hide");
	
	modal.modal("show");
};
sp.groups.edit = function(options) {
	options = options || {};
	options.id = options.id || -1;
	options.show = options.show || function(){};
	options.shown = options.shown || function(){};
	options.hide = options.hide || function(){};
	options.hidden = options.hidden || function(){};
	if(options.id==-1)
	{
		console.error("[sp][sp.users.modal()] invalid user id");
		return false;
	}
	var group = sp.db.groups.get_details(options.id);
	$(".sp-groups-edit").remove();
	var modal_id = "sp-groups-edit-"+sp.groups.last_id++;
	var modal = $('\
	<div class="modal sp-groups-edit" id="'+modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+modal_id+'-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<h4 class="modal-title" id="'+modal_id+'-label">Editar grupo</h4>\
				</div>\
				<div class="modal-body"><div class="text-center"><i class="fa fa-spin fa-cog fa-3x" style="color:rgba(0,0,0,.2)"></i><br>A carregar informações ... </div></div>\
				<div class="modal-footer">\
					<button type="button" class="btn btn-primary sp-group-edit-form-confirm"><i class="fa fa-check"></i> Confirmar</button>\
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>\
				</div>\
			</div>\
		</div>\
	</div>');
	modal.modal({show:false});
	modal.on('show.bs.modal',options.show);
	modal.on('shown.bs.modal',options.shown);
	modal.on('hide.bs.modal',options.hide);
	modal.on('hidden.bs.modal',options.hidden);
	
	modal.on('show.bs.modal',function(){$("html").addClass("modal-open");}).on('hide.bs.modal',function(){$("html").removeClass("modal-open");});
	
	var str = '';
	str += '<h5 class="sp-header"><i class="fa fa-info"></i> Informações gerais</h5>';
	str += '<table class="table table-striped table-bordered sp-group-edit-form">';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-group-edit-form-nome" class="control-label"><b>Nome</b></label></th>';
	str += '		<td><input type="text" class="form-control" id="sp-group-edit-form-nome" placeholder="Nome" value="'+group.nome+'"></td>';
	str += '	</tr>';
    str += '	<tr class="form-group">';
    str += '		<th style="vertical-align:middle;"><label for="sp-group-edit-form-descricao" class="control-label"><b>Descrição</b></label></th>';
    str += '		<td><input type="text" class="form-control" id="sp-group-edit-form-descricao" placeholder="Descrição" value="'+group.descricao+'"></td>';
    str += '	</tr>';
    str += '	<tr class="form-group">';
    str += '		<th style="vertical-align:middle;"><label for="sp-group-edit-form-lista_distribuicao" class="control-label"><b>Lista distribuição</b></label></th>';
    str += '		<td><input type="text" class="form-control" id="sp-group-edit-form-lista_distribuicao" placeholder="E-mails separados por ;" value="'+(group.lista_distribuicao?group.lista_distribuicao:"")+'"></td>';
    str += '	</tr>';
	str += '	<tr class="form-group">';
	str += '		<th style="vertical-align:middle;"><label for="sp-group-edit-form-tipo" class="control-label"><b>Tipo</b></label></th>';
	str += '		<td>';
	str += '			<label style="margin-right:10px;"><input type="radio" name="sp-group-edit-form-tipo" value="0" '+(group.tipo==0?"checked":"")+' /> <span class="label label-warning">Administradores</span></label>';
	str += '			<label style="margin-right:10px;"><input type="radio" name="sp-group-edit-form-tipo" value="1" '+(group.tipo==1?"checked":"")+' /> <span class="label label-primary">Vendedores</span></label>';
	str += '			<label><input type="radio" name="sp-group-edit-form-tipo" '+(group.tipo==2?"checked":"")+' value="2" /> <span class="label label-success">Clientes</span></label>';
	str += '		</td>';
	str += '	</tr>';
	str += '</table>';
	str += '<div id="sp-group-edit-products-container">';
	str += '	<h5 class="sp-header"><i class="fa fa-list"></i> Produtos visíveis</h5>';
	str += '	<button id="sp-group-edit-products" class="btn btn-default"><i class="fa fa-check-square-o"></i> Seleccionar</button>';
	str += '	<span style="margin-left:10px;" id="sp-group-edit-products-selected" class="text-muted">Todos os produtos são visíveis</span>';
	str += '	<input type="hidden" id="sp-group-edit-products-selected-input" value="'+group.produtos+'" />';
	str += '</div>';
	str += '<div id="sp-group-edit-clients-container" style="margin-top:20px;">';
	str += '	<h5 class="sp-header"><i class="fa fa-group"></i> Clientes visíveis</h5>';
	str += '	<button id="sp-group-edit-clients" class="btn btn-default"><i class="fa fa-check-square-o"></i> Seleccionar</button>';
	str += '	<span style="margin-left:10px;" id="sp-group-edit-clients-selected" class="text-muted">Todos os clientes são visíveis</span>';
	str += '	<input type="hidden" id="sp-group-edit-clients-selected-input" value="'+group.clientes+'" />';
	str += '</div>';
	
	modal.find(".modal-body").html(str);
	
	modal.find(".modal-body input[name='sp-group-edit-form-tipo']").change(function(){
		str = '';
		str += '				<option value="">Seleccionar</option>';
		for (var i in sp.db.groups.list)
		{
			if($(this).val()==sp.db.groups.list[i].tipo)
			str += '				<option class="sp-group-group-type sp-group-group-type-'+sp.db.groups.list[i].tipo+'" value="'+sp.db.groups.list[i].id+'">'+sp.db.groups.list[i].nome+'</option>';
		}
		modal.find(".modal-body #sp-group-edit-form-grupo").html(str);
	});
	modal.find(".modal-body input[name='sp-group-edit-form-tipo']:checked").trigger("change");

	modal.find(".modal-body #sp-group-edit-products").on("click",function(){
		sp.product.select({
			selected: modal.find(".modal-body #sp-group-edit-products-selected-input").val().split(","),
			multiple: true,
			confirm:function(selected){
				if(selected.length>0)
				{
					$("#sp-group-edit-products-selected").html("<a href=''>"+selected.length+" produtos seleccionados</a>");
					if(selected.length==1)
					{
						$("#sp-group-edit-products-selected").html("<a href=''>"+selected.length+" produto seleccionado</a>");
					}
					$("#sp-group-edit-products-selected a").on("click",function(e) {
						e.preventDefault();
						modal.find(".modal-body #sp-group-edit-products").trigger("click");
					});
				}
				else
				{
					modal.find(".modal-body #sp-group-edit-products-selected").html("Todos os produtos são permitidos");
				}
				modal.find(".modal-body #sp-group-edit-products-selected-input").val(selected);
			}
		});
	});
	if((group.produtos).length>0)
	{
		modal.find(".modal-body #sp-group-edit-products-selected").html("<a href=''>"+(group.produtos).length+" produtos seleccionados</a>");
		if((group.produtos).length==1)
		{
			modal.find(".modal-body #sp-group-edit-products-selected").html("<a href=''>"+(group.produtos).length+" produto seleccionado</a>");
		}
		modal.find(".modal-body #sp-group-edit-products-selected a").on("click",function(e) {
			e.preventDefault();
			modal.find(".modal-body #sp-group-edit-products").trigger("click");
		});
	}
	else
	{
		modal.find(".modal-body #sp-group-edit-products-selected").html("Todos os produtos são permitidos");
	}

	modal.find(".modal-body #sp-group-edit-clients").on("click",function(){
		sp.client.select({
			selected: modal.find(".modal-body #sp-group-edit-clients-selected-input").val().split(","),
			multiple: true,
			confirm:function(selected){
				if(selected.length>0)
				{
					modal.find(".modal-body #sp-group-edit-clients-selected").html("<a href=''>"+selected.length+" clientes seleccionados</a>");
					if(selected.length==1)
					{
						modal.find(".modal-body #sp-group-edit-clients-selected").html("<a href=''>"+selected.length+" cliente seleccionado</a>");
					}
					modal.find(".modal-body #sp-group-edit-clients-selected a").on("click",function(e) {
						e.preventDefault();
						modal.find(".modal-body #sp-group-edit-clients").trigger("click");
					});
				}
				else
				{
					modal.find(".modal-body #sp-group-edit-clients-selected").html("Todos os clientes são permitidos");
				}
				modal.find(".modal-body #sp-group-edit-clients-selected-input").val(selected);
			}
		});
	});
	if((group.clientes).length>0)
	{
		modal.find(".modal-body #sp-group-edit-clients-selected").html("<a href=''>"+(group.clientes).length+" clientes seleccionados</a>");
		if((group.clientes).length==1)
		{
			modal.find(".modal-body #sp-group-edit-clients-selected").html("<a href=''>"+(group.clientes).length+" cliente seleccionado</a>");
		}
		modal.find(".modal-body #sp-group-edit-clients-selected a").on("click",function(e) {
			e.preventDefault();
			modal.find(".modal-body #sp-group-edit-clients").trigger("click");
		});
	}
	else
	{
		modal.find(".modal-body #sp-group-edit-clients-selected").html("Todos os clientes são permitidos");
	}
	
	modal.find(".modal-body input[name='sp-group-edit-form-tipo']").change(function(){
		var selected = $(this).val();
		if(selected == sp.db.users.types.client) {
			modal.find(".modal-body #sp-group-edit-products-container").show();
			modal.find(".modal-body #sp-group-edit-clients-container").hide();
		}
		else if(selected == sp.db.users.types.sales) {
			modal.find(".modal-body #sp-group-edit-products-container").show();
			modal.find(".modal-body #sp-group-edit-clients-container").show();
		}
		else {
			modal.find(".modal-body #sp-group-edit-products-container").hide();
			modal.find(".modal-body #sp-group-edit-clients-container").hide();
		}
	});
	modal.find(".modal-body input[name='sp-group-edit-form-tipo']:checked").trigger("change");
	
	modal.find(".modal-footer .sp-group-edit-form-confirm").on("click",function(){
		var nome 		= modal.find(".modal-body #sp-group-edit-form-nome");
        var descricao 	= modal.find(".modal-body #sp-group-edit-form-descricao");
        var lista_distribuicao 	= modal.find(".modal-body #sp-group-edit-form-lista_distribuicao");
		var tipo 		= modal.find(".modal-body input[name='sp-group-edit-form-tipo']:checked").val();
		var clientes 	= modal.find(".modal-body #sp-group-edit-clients-selected-input").val();
		var produtos	= modal.find(".modal-body #sp-group-edit-products-selected-input").val();
		
		modal.find(".modal-body .sp-group-edit-form .has-error").removeClass("has-error");
		modal.find(".modal-body .sp-group-edit-form .error-message").remove();
		
		var error_counter = 0;
		if(nome.val()==="") {
			nome.closest(".form-group").addClass("has-error");
			nome.after('<small class="error-message text-danger">Campo obrigatório</small>')
			error_counter++;
		}
		if(descricao.val()==="") {
			descricao.closest(".form-group").addClass("has-error");
			descricao.after('<small class="error-message text-danger">Campo obrigatório</small>')
			error_counter++;
		}
		
		if(error_counter>0) return false;
		
		nome = nome.val();
        descricao = descricao.val();
        lista_distribuicao = lista_distribuicao.val();
		
		bootbox.dialog({
			message: "<div class='alert alert-warning'><i class='fa fa-warning'></i> Confirmar a edição do grupo?</div>",
			title: "Confirmar",
			buttons: {
				success: {
					label: "<i class='fa fa-check'></i> Confirmar",
					className: "btn-primary",
					callback: function() {

						sp.ws.put({
							url:"groups",
							data: {
								"id":options.id,
								"nome":nome,
								"descricao":descricao,
                                "lista_distribuicao":lista_distribuicao,
								"tipo":tipo,
								"clientes":clientes,
								"produtos":produtos
							},
							callbacks:{
								"200":function(data,status,jqXHR){
									toastr.success(data);
									window.location = "#users";
									location.reload();
								},
								"400":function(data,status,jqXHR){
									toastr.error("Existem campos obrigatórios por preencher");
									sp.loading.hide();
								},
								"409":function(data,status,jqXHR){
									toastr.error(data.responseText);
								},
								"error":function(data,status,jqXHR){
									console.error(data.responseText);
								}
							}
						});
					}
				},
				danger: {
					label: "<i class='fa fa-times'></i> Cancelar",
					className: "btn-default"
				}
			}
		});
	});
	
	$(".modal:visible").modal("hide");
	
	modal.modal("show");
};

sp.debug.register("end");